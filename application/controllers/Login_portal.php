<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_portal extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/M_sekolah', '', TRUE);
	}

	function index()
	{
		$data['sekolah'] = $this->M_sekolah->get_sekolah()->result();
		if ($this->M_sekolah->is_logged_in()) {
			redirect('beranda');
		} else {
			$this->form_validation->set_rules('username', 'Username', 'callback_login_check');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

			if ($this->form_validation->run() == FALSE) {
				$data['username'] = $this->input->post('username');
				$this->load->view('login', $data);
			} else
				redirect('beranda');
		}
	}

	function get_autocomplete()
	{
		// if (isset($_GET['term'])) {
		// 	$result = $this->M_sekolah->search_blog($_GET['term']);
		// 	if (count($result) > 0) {
		// 		foreach ($result as $row)
		// 			$arr_result[] = $row->nm_sekolah;
		// 		echo json_encode($arr_result);
		// 	} else {
		// 		$arr_result['suggestions'][] = array(
		// 			'value' => 'Data Kosong / Tidak ditemukan',
		// 			'nm_sekolah' => ''
		// 		);
		// 	}
		// }

		$keyword = $this->input->get("query");
		$data = $this->M_sekolah->search_blog($keyword);

		$arr = [];
		$arr['suggestions'] = [];
		foreach ($data as $row) {
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value' => $row->nm_sekolah
			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

	function login_check($username)
	{
		$password = $this->input->post("password");

		$user = $this->M_sekolah->get_user($username)->result();
		if (count($user) == 0) {
			$this->form_validation->set_message('login_check', '<p class="px-2" style="color:red; margin-left:27px;">Username/Password salah!</p>');
			return false;
		} else {
			foreach ($user as $row) {
				$hashed = $row->password;
				$userid = $row->userid;
			}

			if ($this->phpass->check($password, $hashed)) {
				$this->session->set_userdata('userid', $userid);
				$get_user = $this->M_sekolah->get_userid($this->input->post('username'));
				$data = array(
					'keterangan' => 'Login',
					'ket_waktu' => date('Y-m-d h:i:s'),
					'userid' => $get_user
				);
				$result = $this->M_sekolah->log_activity($data);
				echo json_encode($result);

				return true;
			} else {
				$this->form_validation->set_message('login_check', '<p class="px-2" style="color:red;">Username/Password salah!</p>');
				return false;
			}
		}
	}

	function ajax_check()
	{
		if ($this->M_sekolah->is_logged_in()) {
			echo json_encode(1);
		} else {
			echo json_encode(0);
		}
	}

	function insert_siswa()
	{
		// $data = array(
		// 	'nisn' => $this->input->post('nisn'),
		// 	'npsn' => $this->input->post('npsn'),
		// 	'nm_sekolah' => $this->input->post('nm_sekolah'),
		// 	'nama' => $this->input->post('nama'),
		// 	'email' => $this->input->post('nisnn'),
		// 	'email_alter' => $this->input->post('emailal'),
		// 	'id_region' => $this->input->post('id_region'),
		// 	'alamat' => $this->input->post('alamat'),
		// 	'tgl_lahir' => $this->input->post('tgl_lahir'),
		// 	'no_hp' => $this->input->post('no_hps'),
		// 	'id_sekolah' => $this->input->post('id_sekolah'),
		// 	'xcreate_user' => null,
		// 	'xcreate_date' => date('Y-m-d h:i:s')
		// );
		// $cekk = $this->M_sekolah->cek_email($data['email'])->num_rows();
		// if ($cekk > 0) {
		// 	$result = array('success' => false, 'msg' => 'Email Sudah Terdaftar!');
		// } else if ($this->input->post('no_hps') == null) {
		// 	$result = array('success' => false, 'msg' => 'Harap Semua Data Diisi');
		// } else if ($this->input->post('emailal') == null) {
		// 	$result = array('success' => false, 'msg' => 'Harap Semua Data Diisi');
		// } else {
		// 	$result = $this->M_sekolah->insert_siswa($data);
		// 	$result = array('success' => true, 'email' => $data['email'], 'npsn' => $data['npsn']);
		// }
		// echo json_encode($result);


		$data = array(
			'nisn' => $this->input->post('nisn'),
			'npsn' => $this->input->post('npsn'),
			'nm_sekolah' => $this->input->post('nm_sekolah'),
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('nisnn'),
			'email_alter' => $this->input->post('emailal'),
			'id_region' => $this->input->post('id_region'),
			'alamat' => $this->input->post('alamat'),
			'tgl_lahir' => $this->input->post('tgl_lahir'),
			'no_hp' => $this->input->post('no_hps'),
			'nama_orangtua' => $this->input->post('nama_ortu'),
			'no_orangtua' => $this->input->post('no_ortu'),
			'email_orangtua' => $this->input->post('email_ortu'),
			'id_sekolah' => $this->input->post('id_sekolah'),
			'nama_orangtua' => $this->input->post('nama_ortu'),
			'no_orangtua' => $this->input->post('no_ortu'),
			'email_orangtua' => $this->input->post('email_ortu'),
			'xcreate_user' => null,
			'xcreate_date' => date('Y-m-d h:i:s')
		);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "http://117.53.46.5/zimbra_api/index.php/api/emailexist");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "emailaccount=" . $data['email']);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$response = ($httpCode == 200) ? true : false;

		curl_close($ch);
		if ($response) {
			$result = array('success' => false, 'msg' => 'Email Sudah Terdaftar!');
		} else {
			// $result = json_encode(array('success' => false, 'msg' => 'Data Email Tidak Ditemukan'));

			$ch2 = curl_init();

			curl_setopt($ch2, CURLOPT_URL, "http://117.53.46.5/zimbra_api/index.php/api/createaccount");
			curl_setopt($ch2, CURLOPT_POST, 1);
			curl_setopt(
				$ch2,
				CURLOPT_POSTFIELDS,
				"emailaddress=" . $data['email'] . "&sn=" . $data['nama'] . "&password=" . $data['npsn']
			);

			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($ch2);
			$httpCode = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
			$response = ($httpCode == 200) ? true : false;

			curl_close($ch2);
			if ($response) {
				$this->M_sekolah->insert_siswa($data);
				// $data1 = array(
				//     'keterangan' => 'create email siswa',
				//     'ket_waktu' => date('Y-m-d h:i:s'),
				//     'userid' => $this->load->get_var('user_info')->userid
				// );
				// $this->M_sekolah->log_activity($data1);
				$result = array('success' => true, 'email' => $data['email'], 'npsn' => $data['npsn'], 'msg' => 'Email Berhasil Didaftarkan!');
				// $result = array('success' => true, 'msg' => 'Email Berhasil Didaftarkan!');
			} else {
				$result = array('success' => false, 'msg' => $result);
				// $result = array('success' => false, 'msg' => 'Terjadi Masalah pada Server Email!');
			}
		}


		echo json_encode($result);
	}

	function cek_data()
	{
		$data = array(
			'nuptk' => $this->input->post('nuptk'),
			'sekolah' => $this->input->post('sekolah')
		);

		$cek = $this->M_sekolah->get_nuptk($data['nuptk'])->num_rows();
		$cekk = $this->M_sekolah->cek_sekolah($data['sekolah'])->num_rows();
		if ($cek > 0) {
			$result = array('success' => false, 'msg' => 'NUPTK Sudah ada');
		} else if ($cekk >= 3) {
			$result = array('success' => false, 'msg' => 'Sekolah sudah melebih batas kuota');
		} else if ($this->input->post('sekolah') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi!');
		} else {
			$result = array('success' => true);
			$data = $this->M_sekolah->cek_sekolah2($data['sekolah'])->row();
			$result['id_sekolah'] = $data->id_sekolah;
			$result['id_region'] = $data->id_region;
		}


		echo json_encode($result);
	}

	function insert_sd()
	{
		$data = array(
			'nuptk' => $this->input->post('nuptk'),
			// 'nik' => $this->input->post('nik'),
			'sekolah' => $this->input->post('sekolah'),
			'nama' => $this->input->post('nama2'),
			'email' => $this->input->post('email2'),
			'no_hp' => $this->input->post('no_hp'),
			'password' => $this->input->post('passs'),
			'id_sekolah' => $this->input->post('id_sekolah'),
			'id_region' => $this->input->post('id_region'),
			'statuss' => '1'
		);
		$data1 = array(
			'name' => $this->input->post('nama2'),
			'username' => $this->input->post('nuptk'),
			'password' => password_hash($this->input->post('passs'), PASSWORD_BCRYPT),
			'roleid' => '9',
			'id_sekolah' => $this->input->post('id_sekolah'),
			'xcreate_date' => date('Y-m-d h:i:s')
		);
		if ($this->input->post('nama2') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		} else if ($this->input->post('email2') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		} else if ($this->input->post('no_hp') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		} else if ($this->input->post('passs') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		} else {
			$result = $this->M_sekolah->insert_dyn_user($data1);
			$result = $this->M_sekolah->insert_sekolah($data);
			$result = array('success' => true, 'nuptk' => $data['nuptk'], 'password' => $data['password'], 'email' => $data['email']);
		}
		echo json_encode($result);
	}


	function cek_data_siswa()
	{
		$data = array(
			'nisn' => $this->input->post('nisn'),
			'npsn' => $this->input->post('npsn')
		);

		$cek = $this->M_sekolah->cek_siswa($data['nisn'])->num_rows();
		$cekk = $this->M_sekolah->cek_npsn($data['nisn'], $data['npsn'])->num_rows();
		if ($cek == 0) {
			$result = array('success' => false, 'msg' => 'NISN Tidak Ditemukan');
		} else if ($cekk > 0) {
			$result = array('success' => true);
			$data = $this->M_sekolah->cek_npsn($data['nisn'], $data['npsn'])->row();
			$result['nama'] = $data->nama;
			$result['nm_sekolah'] = $data->nm_sekolah;
			$result['id_region'] = $data->id_region;
			$result['nisn'] = $data->nisn;
			$result['npsn'] = $data->npsn;
			$result['alamat'] = $data->alamat;
			$result['tgl_lahir'] = $data->tgl_lahir;
			$result['id_sekolah'] = $data->id_sekolah;
		} else {
			$result = array('success' => false, 'msg' => 'NPSN Tidak Cocok');
		}

		echo json_encode($result);
	}
}
