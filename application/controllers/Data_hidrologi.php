<?php
defined('BASEPATH') or exit('No direct script access allowed');

// require_once("Secure_area.php");
class Data_hidrologi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin/M_user', '', TRUE);
        $this->load->model('admin/Mdata_pos', '', TRUE);
        $this->load->helper('pdf_helper');
    }

    public function index()
    {
        $data['title'] = 'Data Hidrologi';
        $this->load->view('data_hidrologi/index', $data);
    }

    public function get_pos(){
        $line  = array();
        $line2 = array();
        $row2  = array();
        $i=1;
		$hasil = $this->Mdata_pos->get_hidrologi()->result();
		foreach ($hasil as $value) {
            $row2['no'] = $i++;
            $row2['kategori'] = $value->kategori;
            $row2['nama_pos'] = $value->nama_pos;
            $row2['download'] = '<a href="'.site_url().'Data_hidrologi/cetak_pdf20/'.$value->id.'" class="btn btn-primary" ><i class="fa fa-eye fa-fw"></i></a>';

            $line2[] = $row2;
        }
        $line['data'] = $line2;

        echo json_encode($line);
    }
    
    public function cetak_pdf20($id)
    {
        $data_hidrologi = $this->Mdata_pos->get_hidrologi_2020($id)->result();
        if ($id != '') {

            foreach($data_hidrologi as $row){
            $konten = "<style type=\"text/css\">
					.table-font-size{
						font-size:9px;
					    }
					.table-font-size1{
						font-size:12px;
					    }
					.table-font-size2{
						font-size:9px;
						margin : 5px 1px 1px 1px;
						padding : 5px 1px 1px 1px;
					    }
					</style>
					
					<table class=\"table-font-size2\" border=\"0\">
						<tr>
							<td width=\"16%\">
								<p align=\"center\">
								</p>
							</td>
								<td  width=\"70%\" style=\" font-size:9px;\"><b><font style=\"font-size:12px\">$row->prov</font></b><br><br>$row->prov
							</td>
							<td width=\"14%\"><font size=\"8\" align=\"right\">$row->prov</font></td>						
						</tr>
						<tr><td></td><td colspan=\"2\"><p align=\"right\" style=\"font-size:10px;\"><b>Pembayaran : <u>" . $row->prov . "</u></b></p></td></tr>
						<tr><td></td><td colspan=\"2\"><p align=\"right\" style=\"font-size:10px;\"><b>No. Antrian :<u>" . $row->prov . "</u></b></p></td></tr>
					</table>
					
					<table>	
							<tr>
								<td colspan=\"3\" ><font size=\"12\" align=\"center\"><u><b>KWITANSI RAWAT JALAN 
								No. " . $row->prov . "" . $row->prov . "</b></u></font></td>
							</tr>	
							<br>		
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td width=\"17%\"><b>Nama</b></td>
								<td width=\"2%\"> : </td>
								<td width=\"37%\">" . strtoupper($row->name) . "</td>
							</tr>
							<tr>
								<td><b>Das</b></td>
								<td> : </td>
								<td>" . strtoupper($row->das) . "</td>
							</tr>
							<tr>
								<td ><b>Tipe Station</b></td>
								<td > : </td>
								<td>" . strtoupper($row->station) . "</td>
							</tr>
							
							<tr>
								<td><b>Provinsi</b></td>
								<td> : </td>
								<td rowspan=\"3\">" . strtoupper($row->prov) . "</td>
							</tr>											
							<tr>
								<td><b>Kota</b></td>
								<td> : </td>
								<td rowspan=\"3\">" . strtoupper($row->city) . "</td>
							</tr>											
							<tr>
								<td><b>Kecamatan</b></td>
								<td> : </td>
								<td rowspan=\"3\">" . strtoupper($row->sub) . "</td>
							</tr>											
							</table>
							<br/><br/>";

            }
            $file_name = "tes.pdf";
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            tcpdf();
            $obj_pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
            $obj_pdf->SetCreator(PDF_CREATOR);
            $title = "";

            // $fontname = TCPDF_FONTS::addTTFfont(FCPATH.'assets/font/Calibri.ttf', 'TrueTypeUnicode', '', 32);

            $obj_pdf->SetTitle($file_name);
            $obj_pdf->SetPrintHeader(false);
            $obj_pdf->SetPrintFooter(false);
            $obj_pdf->SetHeaderData('', '', $title, '');
            $obj_pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $obj_pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            $obj_pdf->SetDefaultMonospacedFont('helvetica');
            $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $obj_pdf->SetMargins('5', '5', '5');
            $obj_pdf->SetAutoPageBreak(TRUE, '10');
            // $obj_pdf->SetFont('courier', '', 10);
            // $obj_pdf->SetFont($fontname, '', 12, '', false);
            $obj_pdf->setFontSubsetting(false);
            $obj_pdf->AddPage();
            ob_start();
            $content = $konten;
            ob_end_clean();
            $obj_pdf->writeHTML($content, true, false, true, false, '');
            $obj_pdf->Output(FCPATH . 'download/' . $file_name, 'FI');
            if (file_exists(FCPATH . 'download/' . $file_name)) {
                $data['cetak_kwitansi'] = '1';
            }
        } else {
            redirect('kasir/pembayaran/kwitansi/', 'refresh');
        }
    }
}
