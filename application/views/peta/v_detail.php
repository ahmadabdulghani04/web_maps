<?php
$this->load->view("layout/header");
?>
<style type="text/css">
    #mapid {
        height: 380px;
    }

    .slider-holder
        {
            width: 800px;
            height: 380px;
            background-color: yellow;
            margin-left: auto;
            margin-right: auto;
            margin-top: 0px;
            text-align: center;
            overflow: hidden;
        }
       
        .image-holder
        {
            width: 2400px;
            background-color: red;
            height: 380px;
            clear: both;
            position: relative;
           
            -webkit-transition: left 2s;
            -moz-transition: left 2s;
            -o-transition: left 2s;
            transition: left 2s;
        }
       
        .slider-image
        {
            float: left;
            margin: 0px;
            padding: 0px;
            position: relative;
            width:1000px;
            height:380px;
        }
       
        #slider-image-1:target ~ .image-holder
        {
            left: 0px;
        }
       
        #slider-image-2:target ~ .image-holder
        {
            left: -800px;
        }
       
        #slider-image-3:target ~ .image-holder
        {
            left: -1600px;
        }
       
        .button-holder
        {
            position: relative;
            top: -20px;
        }
       
        .slider-change
        {
            display: inline-block;
            height: 10px;
            width: 10px;
            border-radius: 5px;
            background-color: brown;
        }
</style>

<div style="width: 50%; float:left">
    <div id="mapid"></div>
</div>

<div style="width: 50%; float:right">
<div class="slider-holder">
        <span id="slider-image-1"></span>
        <span id="slider-image-2"></span>
        <span id="slider-image-3"></span>
        <div class="image-holder">
            <?php foreach($dok as $r) {?>
            <img src="<?=base_url()?>assets/uploads/<?=$r->dokumentasi_gambar?>" class="slider-image" />
            <?php } ?>
        </div>
        <div class="button-holder">
            <a href="#slider-image-1" class="slider-change"></a>
            <a href="#slider-image-2" class="slider-change"></a>
            <a href="#slider-image-3" class="slider-change"></a>
        </div>
    </div>
</div>

<div>
    <ul>
        <?php foreach ($bidang as $r) {?>
            <li><b>Nama Bidang : <?=$r->bidang_nama?></li>
            <li>Keterangan : <?=$r->bidang_keterangan?></b></li>
        <?php }?>
    </ul>
</div>

<?php
$this->load->view('layout/footer');
?>

<script src="<?= site_url(); ?>assets/leaflet/leaflet.js"></script>
<script type="text/javascript">

var map = L.map('mapid').setView([-6.9321798, 107.5930735], 13);
    var base_url = "<?= base_url() ?>";
    var v_kode = "<?= $kode?>";

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        $.getJSON(base_url + "assets/geojson/map.geojson", function(data) {
        geoLayer = L.geoJson(data, {
            style: function(feature) {

                var kode = parseFloat(feature.properties.kode);

                if (kode == v_kode) {
                    return {
                    fillOpacity: 0.5,
                    fillColor: "#ea300b",
                    weight: 4,
                    opacity: 1,
                    color: "#11f43e "
                };

                } else {
                    return {
                        fillOpacity: 0.0,
                        weight: 1,
                        opacity: 1,
                        color: "#4286f4"
                    };
                 }
            },
            onEachFeature: function(feature, layer) {
                var kode = feature.properties.kode;
                var latt = parseFloat(feature.properties.latitude);
                var longg = parseFloat(feature.properties.longtitude);

                if (kode == v_kode) {
                    map.flyTo([latt, longg], 15, {
                        animate: true,
                        duration: 1
                    });
                    var center = getCentroid(feature.geometry.coordinates[0]);
                    L.marker([center[1],center[0]]).addTo(map);
                }

            }
        }).addTo(map);
    });

    var getCentroid = function (coord)
    {
        var center = coord.reduce(function (x,y) {
            return [x[0] + y[0]/coord.length, x[1] + y[1]/coord.length]
        }, [0,0])
        return center;
    }

</script>