<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="PT. INTI KONTEN INDONESIA">
    <meta name="keywords" content="klinik, klinik pratama, intens, pt intens, pt inti konten indonesia, email, siswa">
    <meta name="author" content="INTENS">
    <title><?= $this->config->item('web_title'); ?></title>
    <link rel="apple-touch-icon" href="<?= site_url(); ?>app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= site_url(); ?>app-assets/images/ico/Logo_PU__RGB_.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/charts/apexcharts.css"> -->
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/extensions/tether-theme-arrows.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/extensions/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/extensions/shepherd-theme-default.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/plugins/forms/validation/form-validation.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>assets/plugins/autocomplete/jquery.autocomplete.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>assets/plugins/clockpicker/dist/jquery-clockpicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>assets/leaflet/leaflet.css">


    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/themes/semi-dark-layout.css">

    <!-- <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/vendors/css/custom.min.css"> -->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/plugins/extensions/toastr.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/pages/dashboard-analytics.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/pages/card-analytics.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>app-assets/css/plugins/tour/tour.css"> -->
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= site_url(); ?>assets/css/style.css">
    <!-- END: Custom CSS-->
    <link rel="stylesheet" href="../dist/MarkerCluster.css" />
    <link rel="stylesheet" href="../dist/MarkerCluster.Default.css" />



</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<!-- <body class="horizontal-layout horizontal-menu 2-columns  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="2-columns"> -->

<body class="horizontal-layout footer-static pace-done horizontal-menu navbar-container menu-expanded" data-open="hover" data-menu="horizontal-menu" data-shepherd-step="step-1">

    <nav id="ts-primary-navigation" class="navbar sticky-top navbar-expand-md  navbar-light">
        <div class="container">
            <a class="navbar-brand" href="index-map-leaflet-fullscreen.html">
                <img src="<?= site_url(); ?>app-assets/images/logo/kemen.png" alt="">
            </a>
            <button class=" navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrimary" aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarPrimary">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="btn btn-white  " href="http://bwssumatera4.higertech.com/login">
                            Home
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-white  " href="http://bwssumatera4.higertech.com/login">
                            Kinerja
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-white  " href="http://bwssumatera4.higertech.com/login">
                            Berita
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-white  " href="<?= site_url('data_hidrologi'); ?>">
                            Data Hidrologi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-primary  " href="<?= site_url('login_portal'); ?>">
                            <i class="fa fa-sign-in-alt small "></i>
                            Login
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- END: Main Menu-->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12">
                    <h3 class="content-header-title float-left"><?= $title; ?></h3>
                    <?php echo buildBreadcrumb($title); ?>
                </div>
            </div>
            <div class="content-body">