<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once("Secure_area.php");
class Data_gis extends Secure_area {
	public function __construct() {
		parent::__construct();

		$this->load->model('admin/M_user','',TRUE);
	}
	
	public function index(){
		$data['title'] = 'Data GIS';
		$this->load->view('data/index',$data);
    }
}