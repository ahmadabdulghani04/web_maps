<?php
$this->load->view("layout/header2");
?>

<div class="card">
  <h5 class="card-header">
    Selamat Datang Di Web GIS
  </h5>
  <div class="card-body">
    <h5 class="card-title">Sistem Informasi Geografis (SIG)</h5>
    <p class="card-text mt-2">By : R-Tech</p>
  </div>
</div>

<div class="row">
        <!-- Horizontal Chart -->
            <div class="col">
                <div class="card">
        <a href="">
                    <div class="card-header">
                    </div>
                    <center>
                        <div class="card-body">
                            <div class="height-200">
                                <i class="fa fa-map-marker fa-5x"></i>
                            <h1 class="mt-2">0</h1>
                            </div>
                    </div>
                    </center>
        </a>
                </div>
            </div>

        <!-- Pie Chart -->
        <div class="col">
                <div class="card">
                <a href="">
                    <div class="card-header">
                    </div>
                    <center>
                        <div class="card-body">
                            <div class="height-200">
                                <i class="fa fa-user fa-5x"></i>
                            <h1 class="mt-2">0</h1>
                            </div>
                    </div>
                    </center>
                    </a>
                </div>
            </div>
            <div class="col">
                <div class="card">
                <a href="">
                    <div class="card-header">
                    </div>
                    <center>
                        <div class="card-body">
                            <div class="height-200">
                                <i class="fa fa-bar-chart fa-5x"></i>
                            <h1 class="mt-2">0</h1>
                            </div>
                    </div>
                    </center>
                    </a>
                </div>
            </div>
            <div class="col">
                <div class="card">
                <a href="">
                    <div class="card-header">
                    </div>
                    <center>
                        <div class="card-body">
                            <div class="height-200">
                                <i class="fa fa-bullhorn fa-5x"></i>
                            <h1 class="mt-2">0</h1>
                            </div>
                    </div>
                    </center>
                </a>
                </div>
            </div>
    </div>

<?php
    $this->load->view('layout/footer');
?>