<?php
$this->load->view('layout/header2');
?>
<link rel="stylesheet" href="<?php echo site_url('assets/dropify/dist/css/dropify.min.css'); ?>">
<section class="content-header">
    <?php
    echo $this->session->flashdata('success_msg');
    ?>
</section>
<div class="card card-outline-info">
    <div class="card-header">
        <h4 class="m-b-0 text-white text-center"></h4>
    </div>
    <div class="card-block p-b-15">
        <!-- <?php echo form_open_multipart('Data_pos/tambah_datapos'); ?>	 -->
        <form action="<?php echo site_url('Data_pos/tambah_datapos'); ?>" method="post" enctype="multipart/form-data">
            <div class="col-lg-10" style="margin: 0 auto;">
                <div class="form-group text-center">
                    <div id='foto_pasien'></div>
                    <input type="hidden" name="foto_blob" id="foto_blob">
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Nama</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="nama" id="nama" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Das</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="das" id="das" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Tipe Station</label>
                    <div class="col-sm-8">
                        <select name="tipe_stat" id="tipe_stat" class="form-control" style="margin-right: 10px;">
                            <?php
                            foreach ($station as $value) {
                                echo "<option id='val' value=" . $value->id . ">" . $value->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Latitude dan Longitude</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Harap Isi</span>
                            </div>
                            <input type="text" aria-label="First name" placeholder="Latitude" class="form-control" name="latitude">
                            <input type="text" aria-label="Last name" placeholder="Longitude" class="form-control" name="longitude">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Provinsi</label>
                    <div class="col-sm-8">
                        <select id="id_prov" class="form-control select2" style="width: 100%" name="id_prov" onchange="ajaxprovinsi(this.value)" required>
                            <option value="">Pilih Provinsi</option>
                            <?php
                            foreach ($province as $row) {
                                echo '<option value="' . $row->id . '">' . $row->name . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Kota</label>
                    <div class="col-sm-8">
                        <select id="id_kota" class="form-control select2" style="width: 100%" name="id_kota" onchange="ajaxkota()" required>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Subdistrict</label>
                    <div class="col-sm-8">
                        <select id="id_sub" class="form-control select2" style="width: 100%" name="id_sub" onchange="ajaxsub()" required>
                            <option value=""> Pilih Sub </option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Village</label>
                    <div class="col-sm-8">
                        <select id="id_vill" class="form-control select2" style="width: 100%" name="id_vill" required>
                            <option value=""> Pilih Village </option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 control-label col-form-label">Gambar</label>
                    <div class="col-sm-4">
                        <input type="file" class="dropify dt-avatar size-100 mb-10 mx-auto" data-default-file="" data-max-file-size="20M" name="filefoto" id="filefoto" data-allowed-file-extensions="jpeg jpg png pdf doc docx" />
                    </div>
                </div>

                <!-- <hr> -->
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="reset" class="btn waves-effect waves-light btn-danger">Reset</button>
                                    <button type="submit" class="btn waves-effect waves-light btn-primary" id="btn-submit">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 	</div>  -->
                <!-- end tab content -->
                <!-- <?php echo form_close(); ?> -->
        </form>
    </div>
</div>
</div>

<?php
$this->load->view('layout/footer');
?>
<script src="<?php echo site_url('assets/dropify/dist/js/dropify.min.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".select2").select2();
        $('.dropify').dropify();
        $('.dropify-fr').dropify({
            messages: {
                default: 'Drag and drop a file here or click',
                replace: 'Drag and drop or click to replace',
                remove: 'SupRemoveprimer',
                error: 'Sorry, this file is too large'
            }
        });
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    })

    function ajaxprovinsi(id) {
        ajaxku = buatajax();
        var url = "<?php echo site_url('Data_pos/data_city'); ?>";
        url = url + "/" + id;
        // url=url+"/"+Math.random();
        ajaxku.onreadystatechange = stateChangedDokter;
        ajaxku.open("GET", url, true);
        ajaxku.send(null);


    }

    function ajaxkota() {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: {
                "id_prov": $('#id_prov').val(),
                "id_kota": $('#id_kota').val()
            },
            url: "<?php echo site_url('Data_pos/data_sub') ?>",
            success: function(response) {
                if (response.success) {
                    $("#id_sub").html(response.data);
                    $("#id_sub").attr('disabled', false);
                } else {
                    $("#id_sub").html('');
                    $("#id_sub").attr('disabled', true);
                }
            }
        });
    }

    function ajaxsub() {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: {
                "id_prov": $('#id_prov').val(),
                "id_kota": $('#id_kota').val(),
                "id_sub": $('#id_sub').val()
            },
            url: "<?php echo site_url('Data_pos/data_vill') ?>",
            success: function(response) {
                if (response.success) {
                    $("#id_vill").html(response.data);
                    $("#id_vill").attr('disabled', false);
                } else {
                    $("#id_vill").html('');
                    $("#id_vill").attr('disabled', true);
                }
            }
        });
    }

    function buatajax() {
        if (window.XMLHttpRequest) {
            return new XMLHttpRequest();
        }
        if (window.ActiveXObject) {
            return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }

    function stateChangedDokter() {
        var data;
        if (ajaxku.readyState == 4) {
            data = ajaxku.responseText;
            if (data.length >= 0) {
                document.getElementById("id_kota").innerHTML = data;
            }
            /*else{
            				document.getElementById("id_kota").value = "<option selected value=\"\">Pilih Kota/Kab</option>";
            				}*/
        }
    }
</script>