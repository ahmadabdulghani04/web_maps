<?php
$this->load->view('layout/header2');
?>
<link rel="stylesheet" href="<?php echo site_url('assets/dropify/dist/css/dropify.min.css'); ?>">
<section class="content-header">
    <?php
    echo $this->session->flashdata('success_msg');
    ?>
</section>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-header">Data GIS
                    <a type="button" class="btn btn-primary" href="<?= site_url('Data_pos/tambah') ?>"><i class="fa fa-plus"></i> Data POS</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-0">
                        <table id="tablePos" class="table table-bordered table-striped zero-configuration dataTable" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Das</th>
                                    <th>Station Type</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Provinsi</th>
                                    <th>Kota</th>
                                    <th>Kecamatan</th>
                                    <th>Kelurahan</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($data_pos as $row) { ?>

                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td><?= $row->name; ?></td>
                                        <td><?= $row->das; ?></td>
                                        <td><?= $row->station_type; ?></td>
                                        <td><?= $row->latitude; ?></td>
                                        <td><?= $row->longitude; ?></td>
                                        <td><?= $row->prov; ?></td>
                                        <td><?= $row->city; ?></td>
                                        <td><?= $row->sub; ?></td>
                                        <td><?= $row->villa; ?></td>
                                        <td><img src="<?php echo base_url() . 'assets/uploads/' . $row->featured_image ?>" class="img-responsive" height="50px"></td>
                                        <td>
                                            <button type='button' class='btn btn-primary btn-sm' data-toggle='modal' data-target='#editModal' onclick='edit_datapos(<?= $row->id ?>)'><i class="fa fa-pencil-square"></i></button>
                                            <button type='button' class='btn btn-danger btn-sm' onclick='delete_datapos(<?= $row->id ?>)'><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_open('Data_pos/edit_datapos'); ?>
<div class="modal fade" id="editModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-success">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ubah Data POS</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <p class="col-sm-3 form-control-label">Judul</p>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="edit_judul" id="edit_judul">
                        <input type="text" class="form-control" name="edit_id" id="edit_id" hidden>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <p class="col-sm-3 form-control-label">Deskripsi</p>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="edit_deskripsi" id="edit_deskripsi">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <p class="col-sm-3 form-control-label">Informasi</p>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="edit_informasi" id="edit_informasi">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <p class="col-sm-3 form-control-label">Gambar</p>
                    <div class="col-sm-6">
                        <input type="file" class="dropify dt-avatar size-100 mb-10 mx-auto" data-default-file="" data-max-file-size="20M" name="edit_gambar" id="edit_gambar" data-allowed-file-extensions="jpeg jpg png pdf doc docx" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <p class="col-sm-3 form-control-label">Latitude & <br>Longitude</p>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="text" class="form-control" name="edit_latitude" id="edit_latitude">
                            <input type="text" class="form-control" name="edit_longitude" id="edit_longitude">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">Edit Data POS</button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>

<form id="insertTindakanMaster">
    <div class="modal fade" id="myModalTindakan" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-success">


            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Titik POS</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body col-md-12">
                    <div class="form-group row">
                        <!-- <div class="col-sm-12"></div> -->
                        <p class="col-sm-2 form-control-label" id="lbl_nama" style="margin-top: 9px; margin-left:15px;">Judul</p>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="judul" id="judul" required>
                        </div>
                    </div>
                </div>
                <div class="modal-body col-md-12">
                    <div class="form-group row">
                        <!-- <div class="col-sm-12"></div> -->
                        <p class="col-sm-2 form-control-label" id="lbl_nama" style="margin-top: 9px; margin-left:15px;">Deskripsi</p>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="deskripsi" id="deskripsi" required>
                        </div>
                    </div>
                </div>
                <div class="modal-body col-md-12">
                    <div class="form-group row">
                        <!-- <div class="col-sm-12"></div> -->
                        <p class="col-sm-2 form-control-label" id="lbl_nama" style="margin-top: 9px; margin-left:15px;">Informasi</p>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="informasi" id="informasi" required>
                        </div>
                    </div>
                </div>
                <div class="modal-body col-md-11" style="margin-bottom: 13px; margin-left: 10px;">
                    <!-- <div class=" form-group row"> -->
                    <!-- <div class="col-sm-12"></div> -->
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Latitude dan Logitude</span>
                        </div>
                        <input type="text" aria-label="First name" placeholder="Latitude" class="form-control" name="latitude">
                        <input type="text" aria-label="Last name" placeholder="Longitude" class="form-control" name="longitude">
                    </div>
                </div>
                <div class="modal-body col-md-12">
                    <div class="form-group row">
                        <!-- <div class="col-sm-12"></div> -->
                        <p class="col-sm-2 form-control-label" id="lbl_nama" style="margin-top: 7px; margin-left:15px;">Gambar</p>
                        <div class="col-sm-5">
                            <input type="file" name="gambar" id="gambar" required class="custom-file-input" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" style="margin-left: 15px;" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                </div>
                <!-- </div> -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
            </div>
        </div>
    </div>
</form>

<?php
$this->load->view('layout/footer');
?>
<script src="<?php echo site_url('assets/dropify/dist/js/dropify.min.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tablePos').dataTable();
        $('.dropify').dropify();
        $('.dropify-fr').dropify({
            messages: {
                default: 'Drag and drop a file here or click',
                replace: 'Drag and drop or click to replace',
                remove: 'SupRemoveprimer',
                error: 'Sorry, this file is too large'
            }
        });
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    })

    function edit_datapos(id) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?php echo base_url('Data_pos/get_data_pos') ?>",
            data: {
                id: id
            },
            success: function(data) {
                $('#edit_id').val(data[0].id);
                $('#edit_judul').val(data[0].judul);
                $('#edit_deskripsi').val(data[0].deskripsi);
                $('#edit_informasi').val(data[0].informasi);
                $('#edit_gambar').html('<img>');
                $('#edit_gambar').attr('src', 'assets/upload/', data[0].gambar);
                $('#edit_latitude').val(data[0].latitude);
                $('#edit_longitude').val(data[0].longitude);
            },
            error: function() {
                alert("error");
            }
        });
    }

    function delete_datapos(id) {
        Swal.fire({
            title: 'Data POS',
            text: 'Yakin akan menghapus data tersebut?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonText: 'Tidak',
            cancelButtonClass: 'btn btn-danger ml-1',
            buttonsStyling: false,
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() . 'Data_pos/delete_pos'; ?>",
                    dataType: "JSON",
                    data: {
                        'id': id
                    },
                    success: function(result) {
                        if (result == true) {
                            Swal.fire("Sukses", "Data berhasil dihapus.", "success");

                        } else {
                            Swal.fire("Error", "Gagal menghapus data.", "error");
                        }
                    },
                    error: function(event, textStatus, errorThrown) {
                        Swal.fire("Error", "Gagal menghapus data.", "error");
                        console.log('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
                    }
                });
            }
        });

    }
</script>