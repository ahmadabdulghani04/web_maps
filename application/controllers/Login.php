<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/M_user','',TRUE);
	}
	
	function index()
	{	   
		if($this->M_user->is_logged_in()){
			redirect('beranda');
		} else {
			$this->form_validation->set_rules('username', 'Username', 'callback_login_check');
    	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			
			if($this->form_validation->run() == FALSE){
				$data['username']=$this->input->post('username');
				$this->load->view('login',$data);
			}else
				redirect('beranda');
		}
		
	}

	function login_check($username)
	{
		$password = $this->input->post("password");	

		$user = $this->M_user->get_user($username)->result();
		if(count($user)==0){
			$this->form_validation->set_message('login_check', '<p class="px-2" style="color:red;">Username/Password salah!</p>');
			return false;
		}else{
			foreach ($user as $row) {
				$hashed = $row->password;
				$userid = $row->userid;
			}
			
			if($this->phpass->check($password, $hashed)) {
				$this->session->set_userdata('userid', $userid);
				return true;
			}else{
				$this->form_validation->set_message('login_check', '<p class="px-2" style="color:red;">Username/Password salah!</p>');
				return false;
			}
		}
	}

	function ajax_check() {
		if($this->M_user->is_logged_in()) {
			echo json_encode(1);
		} else {
			echo json_encode(0);
		}
	}
}

?>