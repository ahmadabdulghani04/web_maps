<?php
$this->load->view('layout/header');
?>

<section class="content">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body table-responsive">
                        <table id="table_nisn" class="display table table-hover table-bordered table-striped" cellspacing="0" style="width:100%" style="display:block;overflow:auto;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NISN</th>
                                    <th>NPSN</th>
                                    <th>Asal Sekolah</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Email Alternatif</th>
                                    <th id="aksi">Aksi</th>
                                </tr>
                            </thead>                       
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$this->load->view("layout/footer");
?>
<script>
    var tabel_detail;
    $(document).ready(function() {
        $('#aksi').attr('hidden', true);
        <?php if ($roleLogin == 9){ ?>
            $('#aksi').attr('hidden', false);
        <?php } ?>
        tabel_detail = $('#table_nisn').DataTable({
          ajax: {
            "url": '<?php echo site_url('nisn/show_data_siswa')?>',
            "type": 'POST',
            "data": function ( d ) {
            },
              },
              columns: [
                { data: "no" },
                { data: "nisn" },
                { data: "npsn" },
                { data: "nm_sekolah" },
                { data: "nama" },
                { data: "email" },
                <?php if($roleLogin == 9){ ?>
                    { data: "email_alter" },
                    { data: "aksi" }
                <?php } else { ?>
                    { data: "email_alter" }
                <?php } ?>
              ]
        });
    });

    function changePassword(nisn, email){
        document.getElementById("reset_pass_"+nisn).innerHTML = '<i class="fa fa-spinner fa-spin"></i> Loading...';
        $.ajax({
            dataType: "JSON",
            type: 'POST',
            url: "<?php echo base_url('nisn/changePass'); ?>",
            data: {'email' : email},
            success: function(response) {
                if (response.success) {
                    document.getElementById("reset_pass_"+nisn).innerHTML = '<i class="fa fa-lock"></i> Reset Password';
                    tabel_detail.ajax.reload();
                    Swal.fire("Sukses",response.msg, "success");
                } else {
                    document.getElementById("reset_pass_"+nisn).innerHTML = '<i class="fa fa-lock"></i> Reset Password';
                    Swal.fire("Error",response.msg, "error");
                }
            }
        });
    }
</script>