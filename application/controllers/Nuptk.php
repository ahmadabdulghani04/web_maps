<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once("Secure_area.php");
class Nuptk extends Secure_area {
	public function __construct() {
		parent::__construct();

		$this->load->model('admin/M_sekolah','',TRUE);
		// $this->load->helper('pdf_helper');
	}
	
	public function index(){
		$data['title'] = 'NUPTK';
		// print_r($this->load->get_var("user_info"));
        $data['data_sekolah'] = $this->M_sekolah->get_user_sekolah()->result();
		$this->load->view('nuptk/index',$data);
    }

    public function change_status_active($id){
        $hasil = $this->M_sekolah->get_sekolah_by_id($id)->result();
        foreach($hasil as $row){
            $data['sekolah'] = $row->sekolah;
        }
        $cek = $this->M_sekolah->cek_sekolah($data['sekolah'])->num_rows();
        if ($cek >= 3) {
            $success = 	'<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <h4 class="text-danger"><i class="fa fa-ban"></i> Sekolah sudah melebihi batas kuota !</h4>
                        </div>';
		} else{
            $success = 	'<div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <h4 class="text-success"><i class="fa fa-ban"></i> Data berhasil diaktifkan </h4>
                        </div>';
                        $this->M_sekolah->update_to_active($id);
                    }
                $this->session->set_flashdata('msg', $success);
        redirect('nuptk');
    }

    public function change_status_nonactive($id){
        $success = 	'<div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <h4 class="text-success"><i class="fa fa-ban"></i> Data berhasil dinonaktifkan</h4>
                        </div>';
        $this->session->set_flashdata('msg', $success);
        $this->M_sekolah->update_to_nonactive($id);
        redirect('nuptk');
    }

}