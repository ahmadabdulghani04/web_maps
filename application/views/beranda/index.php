<?php
$this->load->view("layout/header2");
?>
<style type="text/css">
  #mapid {
    height: 430px;
  }
</style>

<div id="mapid"></div>
<div class="modal fade" id="modalStationDetail" role="dialog" aria-labelledby="myModalLabelAdd" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form class="form form-horizontal" action="#">
        <div class="modal-header" style="background-color: #f4f7f6;">
          <h5 class="modal-title">Detail Informasi POS</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body py-3 px-4">
          <div class="row">
            <div class="col-md-12">
              <!-- <div class="card" style="background-color: #fcfcfc;border-radius: .25rem;">
                                    <div class="card-content">
                                        <div class="card-body"> -->
              <div class="d-flex justify-content-start mb-3">
                <div class="image-container">
                  <img src="<?php echo base_url() . 'assets/uploads/1.jpg'; ?>" id="imgProfile" style="width: 150px; height: 150px" class="img-thumbnail" />
                </div>
                <div class="userData ml-3">
                  <figure class="ts-item__info">
                    <h4 class="mb-1" id="detailStationName"></h4>
                    <!-- <input type="text" id="id_nama" name="id_nama"> -->
                    <!-- <aside> -->
                    <!-- <dl class="list-inline row">
                                                    <dt class="list-inline-item col-3">Jenis Pos</dt>
                                                    <dd class="list-inline-item col-9 id="detailStationType"></dd>
                                                    <dt class="list-inline-item">DAS</dt>
                                                    <dd class="list-inline-item" id="detailDas"></dd>
                                                </dl> -->
                    <dl>
                      <dt>Jenis Pos</dt>
                      <dd id="detailStationType"></dd>
                      <dt>DAS</dt>
                      <dd id="detailDas"></dd>
                    </dl>
                    <!-- </aside> -->
                  </figure>
                </div>
                <div class="ml-auto">
                  <input type="button" class="btn btn-primary d-none" id="btnDiscard" value="Discard Changes" />
                </div>
              </div>
              <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-information-tab" data-toggle="tab" href="#nav-information" role="tab" aria-controls="nav-information" aria-selected="true">LOKASI POS</a>
                  <a class="nav-item nav-link" id="nav-data-tab" data-toggle="tab" href="#nav-data" role="tab" aria-controls="nav-data" aria-selected="false">TABEL DATA</a>
                  <a class="nav-item nav-link" id="nav-grafik-tab" data-toggle="tab" href="#nav-grafik" role="tab" aria-controls="nav-grafik" aria-selected="false">GRAFIK</a>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-information" role="tabpanel" aria-labelledby="nav-information-tab">
                  <dl class="row">
                    <dt class="col-3 col-lg-2">Koordinat</dt>
                    <dt class="col-1 p-0 text-center ml-3 ml-3" style="max-width: 20px;">:</dt>
                    <dd class="col-8" id="detailLatLng"></dd>

                    <dt class="col-3 col-lg-2">Provinsi</dt>
                    <dt class="col-1 p-0 text-center ml-3" style="max-width: 20px;">:</dt>
                    <dd class="col-8" id="detailProvince"></dd>

                    <dt class="col-3 col-lg-2">Kabupaten/Kota</dt>
                    <dt class="col-1 p-0 text-center ml-3" style="max-width: 20px;">:</dt>
                    <dd class="col-8" id="detailCity"></dd>

                    <dt class="col-3 col-lg-2">Kecamatan</dt>
                    <dt class="col-1 p-0 text-center ml-3" style="max-width: 20px;">:</dt>
                    <dd class="col-8" id="detailSubdistrict"></dd>

                    <dt class="col-3 col-lg-2">Kelurahan/Desa</dt>
                    <dt class="col-1 p-0 text-center ml-3" style="max-width: 20px;">:</dt>
                    <dd class="col-8" id="detailVillage"></dd>
                  </dl>
                </div>
                <div class="tab-pane fade" id="nav-data" role="tabpanel" aria-labelledby="nav-data-tab">
                  <div class="row">
                    <div class="col-12 col-sm-6 col-lg-4">
                      <label class="font-size-12" for="periode">Tanggal</label>
                      <fieldset class="form-group">
                        <input type="text" class="form-control font-size-12" id="periode" name="periode">
                      </fieldset>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                      <label class="font-size-12" for="time_type">Waktu</label>
                      <fieldset class="form-group">
                        <select class="form-control font-size-12" id="time_type">
                          <option value="minute">5 Menit</option>
                          <option value="hour">Jam-Jaman</option>
                          <option value="day">Harian</option>
                        </select>
                      </fieldset>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                      <label>&nbsp;</label>
                      <fieldset class="form-group">
                        <button type="button" class="btn btn-primary font-size-12" id="btnDetailData"><i class="fa fa-search"></i> Lihat Data</button>
                      </fieldset>
                    </div>
                    <div class="table-responsive m-t-0">
                      <table id="tableduga" class="table table-bordered table-striped zero-configuration dataTable" style="width: 100%">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Pos</th>
                            <th>Tanggal</th>
                            <th>Jam</th>
                            <th>CH (mm)</th>
                            <th>Intensitas</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>ARR Daik-LINGGA</td>
                            <td>2020-09-10</td>
                            <td>21:4</td>
                            <td>0</td>
                            <td>-</td>
                          </tr>
                          <tr>
                            <td>1</td>
                            <td>ARR Bendungan Sei Gong</td>
                            <td>2020-09-10</td>
                            <td>21:4</td>
                            <td>0</td>
                            <td>-</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm font-size-12 mb-0 w-100 w-100" id="tableDetailData">
                      <thead></thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane fade" id="nav-grafik" role="tabpanel" aria-labelledby="nav-grafik-tab">
                  <div id="chartdiv"></div>
                </div>
              </div>
              <!-- </div>
                                    </div>
                                </div> -->
            </div>
          </div>
        </div>
        <div class="modal-footer" style="background-color: #f4f7f6;">
          <button type="button" class="btn btn-danger" data-dismiss="modal" style="font-size: 14px;">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<br><br><br>
<section id="basic-horizontal-layouts">
  <div class="row">
    <div class="col-md-6 col-12">
      <div class="card">
        <div class="card-header bg-info pb-1">
          <h3 class="card-title text-center text-white"><i class="fa fa-water"> Pos Duga Air</i></h3>
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="row">
              <div class="table-responsive m-t-0">
                <table id="tablePos" class="table table-bordered table-striped zero-configuration dataTable" style="width: 100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Pos</th>
                      <th>Tanggal</th>
                      <th>Jam</th>
                      <th>TMA (m)</th>
                      <th>Debit (m3/s)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>AWLR Kolong Pongkar</td>
                      <td>2020-09-10</td>
                      <td>21:4</td>
                      <td>6.39</td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>AWLR Sei Baloi</td>
                      <td>2020-09-10</td>
                      <td>21:4</td>
                      <td>6.39</td>
                      <td>-</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6 col-12">
      <div class="card">
        <div class="card-header bg-primary pb-1">
          <h3 class="card-title text-center text-white"><i class="fa fa-cloud-rain"> Pos Curah Hujan</i></h3>
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="row">
              <div class="table-responsive m-t-0">
                <table id="tableCurah" class="table table-bordered table-striped zero-configuration dataTable" style="width: 100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Pos</th>
                      <th>Tanggal</th>
                      <th>Jam</th>
                      <th>CH (mm)per 5 Menit</th>
                      <th>CH (mm)Data Harian</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>ARR Daik-LINGGA</td>
                      <td>2020-09-10</td>
                      <td>21:4</td>
                      <td>0</td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>ARR Bendungan Sei Gong</td>
                      <td>2020-09-10</td>
                      <td>21:4</td>
                      <td>0</td>
                      <td>-</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-header bg-warning pb-1">
          <h3 class="card-title text-center text-white"><i class="fa fa-cloud-rain"> Pos Duga Air & Curah Hujan</i></h3>
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-sm font-size-12 mb-0 w-100" id="awlrArrTable">
                  <thead>
                    <tr>
                      <th class="text-center" scope="col" rowspan="2" style="vertical-align : middle;">No.</th>
                      <th class="text-center" scope="col" rowspan="2" style="vertical-align : middle;">Nama Pos</th>
                      <th class="text-center" scope="col" rowspan="2" style="vertical-align : middle;">Tanggal</th>
                      <th class="text-center" scope="col" rowspan="2" style="vertical-align : middle;">Jam</th>
                      <th class="text-center" scope="col" colspan="2">Duga Air</th>
                      <th class="text-center" scope="col" colspan="2">Curah Hujan</th>
                    </tr>
                    <tr>
                      <th class="text-center" scope="col">TMA</th>
                      <th class="text-center" scope="col">Debit</th>
                      <th class="text-center" scope="col">CH (mm)</th>
                      <th class="text-center" scope="col">Intensitas</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>ARR Daik-LINGGA</td>
                      <td>2020-09-10</td>
                      <td>21:4</td>
                      <td>0</td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>ARR Bendungan Sei Gong</td>
                      <td>2020-09-10</td>
                      <td>21:4</td>
                      <td>0</td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer" style="font-size: 12px;">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group mb-3 mb-md-0">
                    <fieldset class="the-fieldset">
                      <legend class="the-legend">Duga Air</legend>
                      <div class="row justify-content-center">
                        <div class="col-auto">
                          <span style="background-color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> : Siaga 1
                        </div>
                        <div class="col-auto">
                          <span style="background-color:orange;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> : Siaga 2
                        </div>
                        <div class="col-auto">
                          <span style="background-color:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> : Siaga 3
                        </div>
                      </div>
                    </fieldset>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group mb-0">
                    <fieldset class="the-fieldset">
                      <legend class="the-legend">Curah Hujan</legend>
                      <div class="row justify-content-center">
                        <div class="col-auto">
                          <span style="background-color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> : Ringan
                        </div>
                        <div class="col-auto">
                          <span style="background-color:blue;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> : Sedang
                        </div>
                        <div class="col-auto">
                          <span style="background-color:orange;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> : Lebat
                        </div>
                        <div class="col-auto">
                          <span style="background-color:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> : Sangat Lebat
                        </div>
                      </div>
                    </fieldset>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12 col-12">
      <div class="card">
        <div class="card-header bg-danger pb-1">
          <h3 class="card-title text-center text-white"><i class="fa fa-cloud-rain"> Pos Klimatologi</i></h3>
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="row">
              <div class="table-responsive m-t-0">
                <table id="tableKlimatologi" class="table table-bordered table-striped zero-configuration dataTable" style="width: 100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Pos</th>
                      <th>Tanggal</th>
                      <th>Jam</th>
                      <th>CH (mm)per 5 Menit</th>
                      <th>CH (mm)Data Harian</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>ARR Daik-LINGGA</td>
                      <td>2020-09-10</td>
                      <td>21:4</td>
                      <td>0</td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>ARR Bendungan Sei Gong</td>
                      <td>2020-09-10</td>
                      <td>21:4</td>
                      <td>0</td>
                      <td>-</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
$this->load->view('layout/footer');
?>
<script src="<?= site_url(); ?>assets/leaflet/leaflet.js"></script>

<script>
  // $('#test').BootSideMenu({side:"left", autoClosefalse});

  $(document).ready(function() {
    $('#tablePos').dataTable();
    $('#tableduga').dataTable();
    $('#tableCurah').dataTable();
    $('#tableduga_curah').dataTable();
    $('#tableKlimatologi').dataTable();
  })

  var map = L.map('mapid').setView([-6.9321798, 107.5930735], 13);
  var base_url = "<?= base_url() ?>";

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }

  ).addTo(map);

  var myFeatureGroup = L.featureGroup().addTo(map).on("click", groupClick);
  var gisMarker;

  $.getJSON(base_url + "peta/data_json", function(data) {
    $.each(data, function(i, field) {
      var v_lat = parseFloat(data[i].latitude);
      var v_long = parseFloat(data[i].longitude);

      // var icon = L.icon({
      //     iconUrl: base_url + 'assets/images/tortillas.png',
      //     iconSize: [30, 30]
      // });



      gisMarker = L.marker([v_lat, v_long]).addTo(myFeatureGroup).bindPopup('<h1><b>' + data[i].name + '</b></h1><p>' + data[i].das + '</p><div><img style="width:100%" src="<?= base_url() ?>assets/uploads/' + data[i].featured_image + '" alt="maptime logo gif" /></div><div style="width:100%;text-align:center;margin-top:10px;"><button class="btn btn-success text-white btn-xs w-100" type="button" data-toggle="modal" data-target="#modalStationDetail" onclick="edit_sd(' + data[i].id + ')")><i class="fa fa-info"> Lihat Detail </i></button></div>', {
        minWidth: 256
      }).openPopup();

      gisMarker.id = data[i].id;

    });
  });

  function detail(id_poli) {
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: "<?php echo base_url('master/poli/get_data_edit_poli') ?>",
      data: {
        id_poli: id_poli
      },
      success: function(data) {
        //alert(data[0].id_poli);
        $('#edit_id_poli').val(data[0].id_poli);
        $('#edit_id_poli_hidden').val(data[0].id_poli);
        $('#edit_nm_poli').val(data[0].nm_poli);
        $('#edit_nm_pokpoli').val(data[0].nm_pokpoli);
        $('#edit_kode_bpjs').val(data[0].poli_bpjs);
      },
      error: function() {
        alert("error");
      }
    });
  }

  function groupClick(event) {
    // alert("Clicked on marker" + event.layer.id);
  }

  $.getJSON(base_url + "assets/geojson/map.geojson", function(data) {
    geoLayer = L.geoJson(data, {
      style: function(feature) {

        var kategori = parseFloat(feature.properties.kategori);
        if (kategori == 1) { //pemukiman
          return {
            fillOpacity: 0.8,
            weight: 1,
            opacity: 0,
            color: "#11f43e "
          };

        } else { // sungai
          return {
            fillOpacity: 0.8,
            weight: 1,
            opacity: 0,
            color: "#4286f4"
          };
        }
      },
      onEachFeature: function(feature, layer) {
        var kode = parseFloat(feature.properties.kode);

        $.getJSON(base_url + "peta/foto/" + kode, function(data) {

          var info_bidang = "<h5 style='text-align:center'>INFO</h5>";
          info_bidang += "<a href='<?= base_url() ?>data_pos/" + kode + "'><img src='<?= base_url() ?>assets/uploads/" + data + "' alt='maptime logo gif' height='180px' width='230px'></a>";
          info_bidang += "<div style='width:100%;text-align:center;margin-top:10px;'><a href='' data-toggle='modal' data-target='#modalStationDetail'> DETAIL </a></div>";

          layer.bindPopup(info_bidang, {
            maxWidth: 230,
            closeButton: true,
            offset: L.point(0, -20)
          });

          layer.on('click', function() {
            layer.openPopup();
          });

        });

      }
    }).addTo(map);
  });

  function edit_sd(id) {
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: "<?php echo base_url('Peta/get_data_detail') ?>",
      data: {
        id: id
      },
      success: function(data) {
        $('#detailStationName').text(data[0].name);
        $('#detailStationType').text(data[0].station);
        $('#detailDas').text(data[0].das);
        $('#detailLatLng').text(data[0].latitude + ' - ' + data[0].longitude);
        $('#detailProvince').text(data[0].prov);
        $('#detailCity').text(data[0].city);
        $('#detailSubdistrict').text(data[0].sub);
        $('#detailVillage').text(data[0].villa);
      },
      error: function() {
        alert("error");
      }
    });
  }
</script>