<?php
$this->load->view("layout/header");

?>


<section class="content-header">
    <?php
    echo $this->session->flashdata('success_msg');
    ?>
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card card-outline-info">
                <div class="card-header border-bottom mx-2 px-0">
                    <h3 class="border-bottom py-1 mb-0 font-medium-2">
                        <i class="fa fa-user mr-50 "></i>Data Siswa
                    </h3>
                    <div align="right">
                        <button class="btn btn-outline-primary" onclick="getexcel_siswa()"><i class="fa fa-cloud-download"></i> Template Siswa</button>
                    </div>
                </div>
                <div class="card-body">
                    <form id="uploadSiswa">
                        <div class="form-group mt-2">
                            <div class="alert alert-danger" role="alert">Data Siswa tidak boleh mengandung simbol atau Tanda Petik ( ' )</div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="fileSiswa" id="fileSiswa" required>
                                <label class="custom-file-label" id="label_file" for="fileSiswa">Cari file Excel Siswa</label>
                            </div>
                        </div>
                        <button class="btn btn-danger" type="submit"><i class="fa fa-cloud-upload"></i> Upload Siswa</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$this->load->view("layout/footer");
?>
<script src="<?=site_url();?>assets/js/numeral.js"></script>
<script type='text/javascript'>
    var site = "<?php echo site_url();?>";
    var tableTindakan, tableTarif;

    $(document).ready(function () {
        $(".select2").select2();
        // $('#label_file').val('Cari file Excel Siswa');
        $('#uploadSiswa').on('submit', function(event){
            event.preventDefault();

            $.ajax({
                url: "<?=base_url('upload/upload_siswa')?>",
                method:"POST",
                data:new FormData(this),
                dataType: "json",
                contentType:false,
                cache:false,
                processData:false,
                beforeSend: function() {
                    loadingSwal();
                },
                success:function(response){
                    if(response.success){
                        Swal.fire("Upload Berhasil","Email yang Berhasil dibuat "+response.berhasil+" akun, gagal dibuat "+response.gagal, "info");  
                        document.getElementById("uploadSiswa").reset();
                        document.getElementById("label_file").innerHTML  = 'Cari file Excel Siswa';
                    }else{
                        alert('Gagal');
                    }

                }
            })
        });
    });

    function getexcel_siswa(){
        window.open(site+'upload/exceldown/siswa', "_blank");
        window.focus();
    }

    function getCaraBayar(carabayar) {
        $('#penjamin').empty();

        $.ajax({
            type:'POST',
            dataType: 'html',
            url:"<?php echo base_url('master/upload/getDetailCB')?>",
            data: {
                carabayar: carabayar
            },
            success: function(data){
                //alert(data);
                $('#penjamin').html(data);
            },
            error: function(){
                alert("error");
            }
        });
    }

    function loadingSwal(){
        Swal.fire({
            title: 'Menyimpan Data!',
            html: 'Harap Menunggu.',
            // timer: 2000,
            // timerProgressBar: true,
            onBeforeOpen: () => {
                Swal.showLoading()
                // timerInterval = setInterval(() => {
                // Swal.getContent().querySelector('b')
                // 	.textContent = Swal.getTimerLeft()
                // }, 100)
            },
            onClose: () => {
                // clearInterval(timerInterval)
            }
        }).then((result) => {
            if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.timer
            ) {
                console.log('I was closed by the timer') // eslint-disable-line
            }
        });
    }

</script>