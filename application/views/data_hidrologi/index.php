<?php
$this->load->view("layout/header");
?>
<section id="basic-horizontal-layouts">
    <div class="row">

        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="table-responsive m-t-0">
                                <table id="tableHidrologi" class="table table-bordered table-striped zero-configuration dataTable" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kategori</th>
                                            <th>Nama Pos</th>
                                            <th>Tahun 2020</th>
                                            <th>Tahun 2019</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$this->load->view('layout/footer');
?>
<script src="<?= site_url(); ?>assets/leaflet/leaflet.js"></script>

<script>
    // $('#test').BootSideMenu({side:"left", autoClosefalse});

    $(document).ready(function() {
        $('#tableHidrologi').dataTable({
            paging: true,
            searching: true,
            destroy: true,
            ajax: {
                "url": '<?php echo site_url('Data_hidrologi/get_pos/') ?>',
                "type": 'POST',
                "data": function(d) {},
            },
            columns: [{
                data: "no"
                },
                {
                data: "kategori"
                },
                {
                data: "nama_pos"
                },
                {
                data: "download"
                },
                {
                data: "nama_pos"
                }
            ]
        });
    })

</script>