<?php
    $this->load->view('layout/header');
?>
<div class="row">
    <div class="col-md-12" id="email">
        <div class="card">
            <div class="card-header">
                Selamat Datang di Web GIS
            </div>
            <br>
            <div class="col-sm-12">
                <h4 class="card-title" id="judul_email">Sistem Informasi Geografis (SIG)</h4>
            </div>
            <div class="card-content">
                <div class="card-body pl-0 ">
                    By : TRIO WEK WEK
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
        <!-- Horizontal Chart -->
            <div class="col" id="jenjang">
                <div class="card">
                    <div class="card-header">
                        <!-- <h4 class="card-title" id="judul_jenjang">Pendaftar berdasarkan Jenjang Sekolah</h4> -->
                    </div>
                    <div class="card-content">
                        <div class="card-body pl-0">
                            <div class="height-200" >
                                <!-- <canvas id="horizontal-bar"></canvas> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Pie Chart -->
            <div class="col" id="aktif">
                <div class="card">
                    <div class="card-header">
                        <!-- <h4 class="card-title" id="judul_aktif">Sekolah yang Aktif</h4> -->
                  </div>
                    <div class="card-content">
                        <div class="card-body pl-0 row">
                            <div class="height-200 col-sm" id="tinggi_aktif">
                                <!-- <canvas id="simple-pie-chart"></canvas>
                                <canvas id="simple-pie-chart2"></canvas> -->
                            </div>
                            <div class="col-sm" id="tinggi_aktif">
                                <!-- <canvas id="simple-pie-chart3"></canvas> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col" id="aktif">
                <div class="card">
                    <div class="card-header">
                        <!-- <h4 class="card-title" id="judul_aktif">Sekolah yang Aktif</h4> -->
                  </div>
                    <div class="card-content">
                        <div class="card-body pl-0 row">
                            <div class="height-200 col-sm" id="tinggi_aktif">
                                <!-- <canvas id="simple-pie-chart"></canvas>
                                <canvas id="simple-pie-chart2"></canvas> -->
                            </div>
                            <div class="col-sm" id="tinggi_aktif">
                                <!-- <canvas id="simple-pie-chart3"></canvas> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col" id="aktif">
                <div class="card">
                    <div class="card-header">
                        <!-- <h4 class="card-title" id="judul_aktif">Sekolah yang Aktif</h4> -->
                  </div>
                    <div class="card-content">
                        <div class="card-body pl-0 row">
                            <div class="height-200 col-sm" id="tinggi_aktif">
                                <!-- <canvas id="simple-pie-chart"></canvas>
                                <canvas id="simple-pie-chart2"></canvas> -->
                            </div>
                            <div class="col-sm" id="tinggi_aktif">
                                <!-- <canvas id="simple-pie-chart3"></canvas> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
<?php
    $this->load->view('layout/footer');
?>
