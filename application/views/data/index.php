<?php
    $this->load->view('layout/header2');
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-header">Data GIS
                    <div class="btn btn-primary fa fa-plus"> Tambah
                    </div>
                </div>
            	<div class="card-body">
					<div class="table-responsive m-t-0">
						<table id="tablePasien" class="table table-bordered table-striped zero-configuration dataTable" style="width: 100%">
            				<thead >
								<tr>
									<th>No</th>
									<th>Title</th>
								  	<th>Description</th>
								  	<th>Information</th>
								  	<th>Image</th>
								  	<th>Latitude Longitude</th>
								  	<th>Aksi</th>
								</tr>
							</thead>
            			</table>
            		</div>
	            </div>
           	</div> 
        </div>
    </div>
</div>
<?php
    $this->load->view('layout/footer');
?>