<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once ("Secure_area.php");
class Nisn extends Secure_area {
    public function __construct() {
		parent::__construct();
		$this->load->model('admin/M_sekolah','',TRUE);
	}

    public function index(){
        $data = array(
            'title' => 'N I S N',
            'roleLogin' => $this->load->get_var("user_info")->roleid
        );
        $this->load->view('nisn/index', $data);
    }

    // public function check_nisn() 
	// {		
	// 	$nisn = $this->input->post('nisn');		
	// 	$result = $this->M_sekolah->get_nisn($nisn);
	// 	if ($result) {
	// 		echo $result;
	// 	} else {
	// 		$result_error = array(
    //     		'metaData' => array('code' => '503','message' => 'Gagal Koneksi.'),
    //     		'response' => ['peserta' => null]
    //   		);
	// 		echo json_encode($result_error);
	// 	}
	// }

	public function show_data_siswa(){
        $roleid = $this->load->get_var("user_info")->roleid;
        if($roleid == 9){
            $line  = array();
            $line2 = array();
            $row2  = array();
            $id_sekolah = $this->load->get_var("user_info")->id_sekolah;
            $hasil = $this->M_sekolah->get_siswa2($id_sekolah)->result();
            $i=1;
            foreach ($hasil as $value) {
                $row2['no'] = $i++;
                $row2['nisn'] = $value->nisn;
                $row2['npsn'] = $value->npsn;
                $row2['nm_sekolah'] = $value->nm_sekolah;
                $row2['nama'] = $value->nama;
                $row2['email'] = $value->email;
                $row2['email_alter'] = $value->email_alter;
                $row2['aksi'] = '<button type="button" class="btn btn-warning" id="reset_pass_'.$value->nisn.'" onclick="changePassword(\''.$value->nisn.'\', \''.$value->email.'\')"><i class="fa fa-lock"></i> Reset Password</button>';

                $line2[] = $row2;
            }
            $line['data'] = $line2;

            echo json_encode($line);
        }else{
            $line  = array();
            $line2 = array();
            $row2  = array();
            $hasil = $this->M_sekolah->get_siswa()->result();
            $i=1;
            foreach ($hasil as $value) {
                $row2['no'] = $i++;
                $row2['nisn'] = $value->nisn;
                $row2['npsn'] = $value->npsn;
                $row2['nm_sekolah'] = $value->nm_sekolah;
                $row2['nama'] = $value->nama;
                $row2['email'] = $value->email;
                $row2['email_alter'] = $value->email_alter;

                $line2[] = $row2;
            }
            $line['data'] = $line2;

            echo json_encode($line);
        }
    }

    function cek_nuptk()
    {
        $data = array(
            'nuptk' => $this->input->post('nuptk'),
        );

        $cek = $this->M_sekolah->get_nuptk($data['nuptk'])->num_rows();
        if($cek > 0){
            $result = array('success' => false, 'msg' => 'Nama Paket Sudah Terdaftar');
        }else{
            $result = array('success' => true, 'msg' => 'Nama Paket Terdaftar');
        }

        echo json_encode($result);
    }

    function changePass(){
     
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://117.53.46.5/zimbra_api/index.php/api/emailexist");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "emailaccount=".$this->input->post('email'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $hasil = curl_exec($ch);
        // $hasil = json_encode($hasil);
        $hasil = json_decode($hasil);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = ($httpCode == 200) ? true : false ;

        curl_close ($ch); 
        if(!$response){
            $result = array('success' => false, 'msg' => 'Email Tidak Ditemukan!');
        }else{
            // $result = json_encode(array('success' => false, 'msg' => 'Data Email Tidak Ditemukan'));
            $data_siswa = $this->M_sekolah->cek_email($this->input->post('email'))->row();
            $ch2 = curl_init();

            curl_setopt($ch2, CURLOPT_URL,"http://117.53.46.5/zimbra_api/index.php/api/setpassword");
            curl_setopt($ch2, CURLOPT_POST, 1);
            curl_setopt($ch2, CURLOPT_POSTFIELDS, "id=". $hasil->data->id ."&password=".$data_siswa->npsn);

            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);

            $hasil2 = curl_exec($ch2);
            $httpCode = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
            $response = ($httpCode == 200) ? true : false ;

            curl_close ($ch2); 
            if($response) {
                $data1 = array(
                    'keterangan' => 'Reset password siswa',
                    'ket_waktu' => date('Y-m-d h:i:s'),
                    'userid' => $this->load->get_var('user_info')->userid
                );
                $this->M_sekolah->log_activity($data1);
                $result = array('success' => true, 'msg' => 'Password berhasil direset!');
            }else{
                $result = array('success' => false, 'msg' => ' Terjadi Masalah pada Server Email!');
            }
        }

        
        echo json_encode($result);

    }
}