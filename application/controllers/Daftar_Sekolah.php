<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_Sekolah extends CI_Controller
{
    public function __construct() {
		parent::__construct();
		$this->load->model('admin/M_sekolah','',TRUE);
	}

    public function index(){
        $data = array(
            'title' => 'Daftar Email Sekolah'
        );
        $this->load->view('daftar/daftar_sekolah', $data);
    }

    function get_autocomplete(){
        // if (isset($_GET['term'])) {
        //     $result = $this->M_sekolah->search_blog($_GET['term']);
        //     if (count($result) > 0) {
        //     foreach ($result as $row)
        //         $arr_result[] = $row->nm_sekolah;
        //         echo json_encode($arr_result);
        //     }
        // }
        $keyword = $this->input->get("query");
        $data = $this->M_sekolah->search_blog($keyword);   

        $arr = [];
		$arr['suggestions']=[];
        foreach($data as $row)
        {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' =>$row->nm_sekolah
            );
        }
        // minimal PHP 5.2
        echo json_encode($arr);
    }

    function cek_data_sekolah()
	{
		$data = array(
			'nuptk' => $this->input->post('nuptk'),
			'sekolah' => $this->input->post('sekolah')
		);

		$cek = $this->M_sekolah->get_nuptk($data['nuptk'])->num_rows();
		$cekk = $this->M_sekolah->cek_sekolah($data['sekolah'])->num_rows();
		if ($cek > 0) {
			$result = array('success' => false, 'msg' => 'NUPTK Sudah ada');
		} else if ($cekk >= 3) {
			$result = array('success' => false, 'msg' => 'Sekolah sudah melebih batas kuota');
		} else if ($this->input->post('sekolah') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi!');
		} else {
			$result = array('success' => true);
			$data = $this->M_sekolah->cek_sekolah2($data['sekolah'])->row();
			$result['id_sekolah'] = $data->id_sekolah;
			$result['id_region'] = $data->id_region;
		}


		echo json_encode($result);
    }
    
    function insert_sekolah()
	{
		$data = array(
			'nuptk' => $this->input->post('nuptk'),
			// 'nik' => $this->input->post('nik'),
			'sekolah' => $this->input->post('sekolah'),
			'nama' => $this->input->post('nama2'),
			'email' => $this->input->post('email2'),
			'no_hp' => $this->input->post('no_hp'),
			'id_sekolah' => $this->input->post('id_sekolah'),
			'id_region' => $this->input->post('id_region'),
			'password' => $this->input->post('passs'),
			'statuss' => '1'
		);
		$data1 = array(
			'name' => $this->input->post('nama2'),
			'username' => $this->input->post('nuptk'),
			'password' => password_hash($this->input->post('passs'), PASSWORD_BCRYPT),
			'roleid' => '9',
			'id_sekolah' => $this->input->post('id_sekolah'),
			'xcreate_date' => date('Y-m-d h:i:s'),
			// 'xcreate_user' => $this->load->get_var("user_info")->username
		);
		$data2 = array(
            'keterangan' => 'create email sekolah',
            'ket_waktu' => date('Y-m-d h:i:s'),
            // 'userid' => $this->load->get_var('user_info')->userid
			);
			
		if ($this->input->post('nama2') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		} else if ($this->input->post('email2') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		} else if ($this->input->post('no_hp') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		} else if ($this->input->post('passs') == null) {
			$result = array('success' => false, 'msg' => 'Harap semua data diisi');
		} else {
			$result = $this->M_sekolah->insert_dyn_user($data1);
			$result = $this->M_sekolah->insert_sekolah($data);
			$result = $this->M_sekolah->log_activity($data2);
			$result = array('success' => true, 'nuptk' => $data['nuptk'], 'password' => $data['password'], 'email' => $data['email']);
		}
		echo json_encode($result);
	}

}