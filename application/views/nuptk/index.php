<?php
$this->load->view('layout/header');
?>

<section class="content">
    <div class="row match-height">
    <div class="col-md-12">
				<?php echo $this->session->flashdata('msg'); ?>
	</div>
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <table id="table_nuptk" class="display table table-hover table-bordered table-striped" cellspacing="0" style="width:100%" style="display:block;overflow:auto;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NUPTK</th>
                                    <th>NIK</th>
                                    <th>Asal Sekolah</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </tr>
                                <tbody>
                                <?php
                                $i = 1;
                                foreach($data_sekolah as $row){
                                    ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $row->nuptk ?></td>
                                    <td><?php echo $row->nik ?></td>
                                    <td><?php echo $row->sekolah ?></td>
                                    <td><?php echo $row->nama ?></td>
                                    <td><?php echo $row->email ?></td>
                                    <td><?php if($row->statuss == 1){?>
                                        <a href="<?php echo site_url('nuptk/change_status_nonactive/').($row->id) ?>" class="btn btn-danger btn-sm">Non Aktif</a> </td>
                                    <?php }else{ ?>
                                        <a href="<?php echo site_url('nuptk/change_status_active/').($row->id) ?>" class="btn btn-success btn-sm">Aktif</a> </td>
                                    <?php } ?>
                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </thead>                       
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$this->load->view("layout/footer");
?>
<script>
    var tabel_detail;
    $(document).ready(function() {
        $('#table_nuptk').DataTable();
    })
</script>