<?php
$this->load->view("layout/header2");
?>
<style type="text/css">
     .swal-wide{
         width:350px !important;
     }
</style>
<section id="basic-horizontal-layouts">
     <div class="row">
          <div class="col-md-8 col-12">
               <div class="form-group mb-4">
                    <div class="form-group row">
                         <div class="col-sm-12">
                              <h2 class="mb-2 text-center">Portal e-Mail Siswa Nasional</b></h2>
                         </div>
                    </div>
                    <p class="text-center">Selamat datang di Portal e-Mail Siswa Nasional.</p>
                    <!-- <img src="assets/images/logo-kemendikbud.png" style="width:93px; height:70px; margin-top:30px; margin-left:80px;" alt=""><br><br> -->
                    <img src="<?=site_url();?>assets/images/siswa.png" class="img-fluid" alt="">
               </div>
          </div>
          <div class="col-md-4 col-12 ">
               <div class="form-group">
                    <div class="form-group row">
                         <div class="col-sm-12">
                              <h2>Buat Akun e-Mail Siswa Disini</h2>
                              <p>*Form pendaftaran khusus <b>siswa</b></p>
                         </div>
                    </div>
                    <form id="input_siswa">
                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <input type="text" class="form-control col-md-10" onkeypress="return inputNumbersOnly(event)" placeholder="NISN" name="nisn" id="nisn" required>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <input type="text" class="form-control col-md-10" placeholder="NPSN" name="npsn" id="npsn" onkeypress="return inputNumbersOnly(event)" maxlength="16" required>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <button type="submit" id="btn_cek_nisn" class="btn btn-primary btn-inline">Check</button><br>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <input type="text" class="form-control col-md-10" placeholder="Nama" name="nama" id="nama" readonly>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <input type="text" class="form-control col-md-10" placeholder="Sekolah" name="nm_sekolah" id="nm_sekolah" readonly>
                              </div>
                         </div>

                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <input type="text" class="form-control col-md-10" placeholder="@siswa.id" name="nisnn" id="nisnn" readonly>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <input type="number" class="form-control col-md-10" placeholder="No Hp" name="no_hps" id="no_hps">
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <input type="email" class="form-control col-md-10" placeholder="Email Alternatif" name="emailal" id="emailal">
                                   <input type="hidden" class="form-control col-md-10" name="id_region" id="id_region">
                                   <input type="text" class="form-control col-md-10" name="alamat" id="alamat" hidden>
                                   <input type="text" class="form-control col-md-10" name="tgl_lahir" id="tgl_lahir" hidden>
                                   <input type="text" class="form-control col-md-10" name="id_sekolah" id="id_sekolah" hidden>
                              </div>
                         </div>
                         <div class="form-group row">
                              <div class="col-sm-12">
                                   <button type="button" onclick="insert_siswa()" class="btn btn-success btn-inline" name="bemail" id="bemail">Buat Email</button><br>
                              </div>
                         </div>
               </div>
          </div>
          </form>
          <form id="input_form">
               <div class="modal fade" id="modal-sd" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-success">
                         <!-- Modal content-->
                         <div class="modal-content">
                              <div class="modal-header">
                                   <h4 class="modal-title" style="width: 100%; text-align: center;">Form Daftar
                                        <button type="button" style="border: none; border-radius: 20px;  height: 25px; width:25px" class="float-right" data-dismiss="modal" onmouseover="this.style.color= 'lightgrey'" onmouseout="this.style.color='grey'">&times;</button></h4>
                              </div>

                              <div class="modal-body">
                                   <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <p class="col-sm-3 form-control-label">NUPTK</p>
                                        <div class="col-sm-7">
                                             <input type="text" onkeypress="return inputNumbersOnly(event)" class="form-control" name="nuptk" id="nuptk" required>
                                        </div>
                                   </div>
                                   <div class="form-group row">
                                        <div class="col-sm-1"></div>
                                        <p class="col-sm-3 mt-1 form-control-label">Sekolah</p>
                                        <div class="col-sm-7">
                                             <input type="search" class="auto_search_sekolah form-control" name="sekolah" id="sekolah" required>
                                        </div>
                                   </div>
                                   <div class="form-group row">
                                        <div class="col-sm-11">
                                             <button type="submit" id="cek" class="btn btn-primary float-right btn-inline mt-1">Check</button><br>
                                        </div>
                                   </div>
                                   <div class="form-group row" id="nma">
                                        <div class="col-sm-1"></div>
                                        <p class="col-sm-3 form-control-label">Nama</p>
                                        <div class="col-sm-7">
                                             <input type="text" class="form-control" name="nama2" id="nama2">
                                        </div>
                                   </div>

                                   <div class="form-group row" id="eml">
                                        <div class="col-sm-1"></div>
                                        <p class="col-sm-3 form-control-label">Email Alternatif</p>
                                        <div class="col-sm-7">
                                             <input type="email" class="form-control" name="email2" id="email2">
                                        </div>
                                   </div>
                                   <div class=" form-group row" id="hp">
                                        <div class="col-sm-1"></div>
                                        <p class="col-sm-3 form-control-label">No HP</p>
                                        <div class="col-sm-7">
                                             <input type="text" onkeypress="return inputNumbersOnly(event)" class="form-control" name="no_hp" id="no_hp">
                                        </div>
                                   </div>
                                   <div class="form-group row" id="pass">
                                        <div class="col-sm-1"></div>
                                        <input type="hidden" class="form-control col-md-10" name="id_sekolah" id="id_sekolah2">
                                        <input type="hidden" class="form-control col-md-10" name="id_region" id="id_region2">
                                        <p class="col-sm-3 form-control-label">Password</p>
                                        <div class="col-sm-7">
                                             <input type="password" class="form-control" name="passs" id="passs">
                                        </div>
                                   </div>

                                   <div class="form-group row">
                                        <div class="col-sm-11">
                                             <button type="button" onclick="insert_sekolah()" id="daftar" class="btn btn-success float-right btn-inline mt-1">Daftar</button><br>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </form>
          <div class="modal fade" id="modal-login" role="dialog" data-backdrop="static" data-keyboard="false">
               <div class="modal-dialog modal-success">
                    <!-- Modal content-->
                    <div class="modal-content">
                         <div class="modal-header">
                              <h4 class="modal-title" style="width: 100%; text-align: center;">Form Login
                                   <button type="button" style="border: none; border-radius: 20px;  height: 25px; width:25px" class="float-right" data-dismiss="modal" onmouseover="this.style.color= 'lightgrey'" onmouseout="this.style.color='grey'">&times;</button></h4>
                         </div>
                         <?php
                         $attributes = array('class' => '', 'id' => 'loginform');
                         echo form_open('login_portal', $attributes);
                         ?>
                         <div class="modal-body">
                              <div class="form-group row">
                                   <div class="col-sm-1"></div>
                                   <p class="col-sm-3 form-control-label mt-1">username</p>
                                   <div class="col-sm-6">
                                        <input type="text" class="form-control" name="username" id="username" required <?php
                                                                                                                        if ($username) {
                                                                                                                             echo 'value="' . $username . '"';
                                                                                                                        } else {
                                                                                                                             echo 'autofocus';
                                                                                                                        }
                                                                                                                        ?>>
                                   </div>
                              </div>
                              <div class="form-group row">
                                   <div class="col-sm-1"></div>
                                   <p class="col-sm-3 form-control-label mt-1">password</p>
                                   <div class="col-sm-6">
                                        <input type="password" class="form-control" name="password" id="password" required <?php
                                                                                                                             if ($username) {
                                                                                                                                  echo 'autofocus';
                                                                                                                             }
                                                                                                                             ?>>
                                   </div>
                              </div>
                              <?php
                              if (validation_errors()) {
                                   echo "<script>
                                   $('#modal-login').modal('show');
                                   </script>";
                                   echo validation_errors();
                              }
                              ?>
                              <div class="form-group row">
                                   <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary float-right btn-inline mt-1">Login</button><br>
                                   </div>
                              </div>
                         </div>
                         <?php echo form_close() ?>
                    </div>
               </div>

          </div>
     </div>

</section>

<?php
$this->load->view("layout/footer");
?>

<script type="text/javascript">
     $(document).ready(function() {
            $('.auto_search_sekolah').autocomplete({
               serviceUrl: '<?php echo site_url();?>Login_portal/get_autocomplete',
          });
        });

     function insert_sekolah() {
          document.getElementById("daftar").innerHTML = '<i class="fa fa-spinner fa-spin"></i> Processing...';
          $.ajax({
               type: "POST",
               url: "<?php echo site_url('login_portal/insert_sd'); ?>",
               dataType: "JSON",
               data: $('#input_form').serialize(),
               success: function(data, result) {
                    var str = "Username : " + data.nuptk + "\n" +
                         "Password : " + data.password + "\n" +
                         "Email Alternatif : " + data.email + "\n" +
                         "*Jangan lupa discreenshoot!" + "\n";
                    if (data.success) {

                         document.getElementById("daftar").innerHTML = 'Tambahkan';
                         $('#modal-sd').modal('hide');
                         $('.modal-backdrop').remove();
                         swal.fire({
                              title: 'Data berhasil disimpan',
                              html: '<pre>' + str + '</pre>',
                              type: 'success',
                              showCancelButton: false,
                              confirmButtonText: 'Ok',
                              confirmButtonClass: 'btn btn-primary',
                              buttonsStyling: false,
                         }).then(function(result) {
                              window.location.reload();
                         });
                    } else {
                         if (data.msg !== null) {
                              document.getElementById("daftar").innerHTML = 'Tambahkan';
                              $('.modal-backdrop').remove();
                              Swal.fire({
                                   type: 'warning',
                                   title: 'Gagal!',
                                   text: data.msg
                              });
                         }
                    }
               },
               error: function(event, textStatus, errorThrown) {
                    document.getElementById("daftar").innerHTML = 'Tambahkan';
                    $('#modal-sd').modal('hide');
                    $('.modal-backdrop').remove();
                    swal.fire("Error", "Gagal menyimpan data.", "error");
                    console.log('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
               }
          });
     }


     function insert_siswa() {
          if($('#no_hps').val()==""){
               swal.fire("Maaf", "Harap Isi No HP.", "error")
          }else if($('#emailal').val()==""){
               swal.fire("Maaf", "Harap Isi Email Alternatif.", "error")
          }else{
               document.getElementById("bemail").innerHTML = '<i class="fa fa-spinner fa-spin"></i> Processing...';
               $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('login_portal/insert_siswa'); ?>",
                    dataType: "JSON",
                    data: $('#input_siswa').serialize(),
                    success: function(result) {
                         // var str = "Username : " + result.email + "\n" +
                         //      "Password : " + result.npsn + "\n" +
                         //      "*Silahkan melakukan verifikasi \n dengan login ke <a href='https://siswa.id/'>https://siswa.id/</a> " + "\n";
                         var str = "Username : " + result.email + "\n" +
                              "Password : " + result.npsn + "\n" +
                              "*Silahkan ubah password anda \n dengan login ke <a href='https://mail.intens.id/' target='_blank'>https://siswa.id/</a>" + "\n" +
                              "lalu masuk ke menu preferences." + "\n";
                         if (result.success) {
                              document.getElementById("bemail").innerHTML = 'Buat Email';
                              $('#modal-sd').modal('hide');
                              $('.modal-backdrop').remove();
                              swal.fire({
                                   title: 'Email Berhasil Dibuat<br>Harap catat email dan password',
                                   html: '<pre>' + str + '</pre>',
                                   type: 'success',
                                   showCancelButton: false,
                                   confirmButtonText: 'Ok',
                                   confirmButtonClass: 'btn btn-primary',
                                   buttonsStyling: false,
                                   customClass: 'swal-wide',
                              }).then(function(result) {
                                   window.location.reload();
                              });
                         } else {
                              if (result.msg !== '') {
                                   document.getElementById("bemail").innerHTML = 'Buat Email';
                                   Swal.fire({
                                        type: 'warning',
                                        title: 'Gagal!',
                                        text: result.msg
                                   }).then(function(result) {
                                        // window.location.reload();
                                   });
                              }
                         }
                    },
                    error: function(event, textStatus, errorThrown) {
                         document.getElementById("bemail").innerHTML = 'Buat Email';
                         $('#modal-sd').modal('hide');
                         $('.modal-backdrop').remove();
                         swal.fire("Error", "Gagal menyimpan data.", "error");
                         console.log('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
                    }
               });
          }
     }


     $(function() {
          $('#nm_sekolah').hide();
          $('#nama').hide();
          $('#nisnn').hide();
          $('#emailal').hide();
          $('#bemail').hide();
          $('#nma').hide();
          $('#eml').hide();
          $('#hp').hide();
          $('#pass').hide();
          $('#daftar').hide();
          $('#no_hps').hide();
     });

     $("#input_form").submit(function(event) {

          event.preventDefault();
          $.ajax({
               url: "<?php echo site_url('login_portal/cek_data') ?>",
               type: 'POST',
               dataType: 'json',
               data: $(this).serialize(),
               success: function(data) {
                    if (data.success) {
                         $('#nuptk').attr('readonly', true);
                         $('#nik').attr('readonly', true);
                         $('#sekolah').attr('readonly', true);
                         $('#cek').hide();
                         $('#nma').show();
                         $('#id_sekolah2').val(data.id_sekolah);
                         $('#id_region2').val(data.id_region);
                         $('#eml').show();
                         $('#hp').show();
                         $('#pass').show();
                         $('#daftar').show();

                    } else {
                         if (data.msg !== '') {
                              Swal.fire({
                                   type: 'warning',
                                   title: 'Gagal!',
                                   text: data.msg
                              });
                         } else {
                              Swal.fire({
                                   type: 'error',
                                   title: 'Oops...',
                                   text: 'Terjadi Kesalahan... Silahkan hubungi Administrator'
                              });
                         }
                    }
               }

          });
     });

     $("#input_siswa").submit(function(event) {

          event.preventDefault();
          $.ajax({
               url: "<?php echo site_url('Login_portal/cek_data_siswa') ?>",
               type: 'POST',
               dataType: 'json',
               data: $(this).serialize(),
               success: function(data) {
                    if (data.success) {
                         $('#nisn').attr('readonly', true);
                         $('#npsn').attr('readonly', true);
                         $('#nm_sekolah').show().val(data.nm_sekolah);
                         $('#nama').show().val(data.nama);
                         // $('#nisnn').show().val(data.nisn + '@siswa.id');
                         $('#nisnn').show().val(data.nisn + '@intens.id');
                         $('#regional').val(data.regional);
                         $('#alamat').val(data.alamat);
                         $('#tgl_lahir').val(data.tgl_lahir);
                         $('#id_sekolah').val(data.id_sekolah);
                         $('#id_region').val(data.id_region);
                         $('#emailal').show();
                         $('#no_hps').show();
                         $('#bemail').show();
                         $('#btn_cek_nisn').hide();

                    } else {
                         if (data.msg !== '') {
                              Swal.fire({
                                   type: 'warning',
                                   title: 'Gagal!',
                                   text: data.msg
                              });
                         } else {
                              Swal.fire({
                                   type: 'error',
                                   title: 'Oops...',
                                   text: 'Terjadi Kesalahan... Silahkan hubungi Administrator'
                              });
                         }
                    }
               }

          });
     });

     function inputNumbersOnly(evt) {
          var charCode = (evt.which) ? evt.which : evt.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57))
               return false;
          return true;
     }
</script>