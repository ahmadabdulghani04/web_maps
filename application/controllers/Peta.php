<?php
defined('BASEPATH') or exit('No direct script access allowed');

// require_once("Se	cure_area.php");
class Peta extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('admin/M_user', '', TRUE);
		$this->load->model('admin/M_sekolah', '', TRUE);
		$this->load->model('admin/Mdata_pos', '', TRUE);
		// $this->load->model('admin/M_peta', '', TRUE);
		// $this->load->helper('pdf_helper');
	}

	public function index()
	{
		$data['title'] = 'Peta';
		$data['data_pos'] = $this->Mdata_pos->get_alldata()->result();
		$this->load->view('peta/index', $data);
	}

	public function data_json()
	{
		$data = $this->db->get('stations')->result();
		echo json_encode($data);
	}

	public function foto($kode = null)
	{
		$data = $this->db->limit(1)->get_where('bidang', array('bidang_kode' => $kode))->row()->bidang_gambar;
		echo json_encode($data);
	}

	public function detail($kode = null)
	{
		$data['title'] = 'Detail Peta';
		$data['kode'] = $kode;
		$data['bidang'] = $this->db->get_where('bidang', array('bidang_kode' => $kode))->result();
		$data['dok'] = $this->db->get_where('dokumentasi', array('dokumentasi_bidang_id' => $kode))->result();
		$this->load->view('peta/v_detail', $data);
	}

	function get_data_detail()
    {
        $id = $this->input->post('id');
        $datajson = $this->Mdata_pos->get_detail($id)->result();
        echo json_encode($datajson);
    }
}
