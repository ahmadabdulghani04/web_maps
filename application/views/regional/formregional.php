<?php
        $this->load->view("layout/header");
?>
	
<?php echo $this->session->flashdata('success_msg'); ?>
<br>
<div class="card card-outline-info">
	<div class="card-header">
        <h4 class="m-b-0 text-white text-center">PENDAFTARAN EMAIL BARU</h4>
    </div>
    <div class="card-block p-b-15">
					<form id="cek_form">
						<div class="col-lg-10" style="margin: 0 auto;">	
                            <div class="form-group row">
								<label class="col-sm-3 control-label col-form-label" id="name">Nama</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="name" id="name" required>
								</div>
							</div>
                            <div class="form-group row">
								<label class="col-sm-3 control-label col-form-label" id="username">Username</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="username" id="username" required>
								</div>
							</div>
                            <div class="form-group row">
                                <p class="col-sm-3 form-control-label  col-form-label">Password</p>
                                <div class="col-sm-6">
                                        <input type="password" class="form-control" name="password" id="password" required>
                                </div>
                            </div>
                            <div class="form-group row">
								<label class="col-sm-3 control-label col-form-label">Regional</label>
								<div class="col-sm-6">
									<input type="search" class="auto_search_region form-control" name="nama_region" id="nama_region" required>
								</div>
							</div>
			<div class="form-actions" id="simpan">
                <div class="row">
                    <div class="col-md-12">
                         <div class="row">
                             <div class="col-md-12 text-center">
								<button type="button" onclick="insert_regional()" class="btn waves-effect waves-light btn-primary" name="btn-submit" id="btn-submit">Simpan</button>
                             </div>
                         </div>
                     </div>
                </div>
            </div>
    </div>
</form>
</div>
</div>
<div class="card">
     <div class="card-content">
     <div class="card-body">
          <table id="table_regional" class="display table table-hover table-bordered table-striped" cellspacing="0" style="width:100%" style="display:block;overflow:auto;">
               <thead>
                    <tr>
                         <th>No</th>
                         <th>Nama</th>
                         <th>Username</th>
                         <th>Region</th>
                    </tr>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach($data_region as $row){
                         ?>
                    <tr>
                         <td><?php echo $i++; ?></td>
                         <td><?php echo $row->name ?></td>
                         <td><?php echo $row->username ?></td>
                         <td><?php echo $row->nama_region ?></td>
                    </tr>
                    <?php
                    }
                    ?>
                    </tbody>
               </thead>                       
          </table>
     </div>
     </div>
</div>

	
<?php 
    $this->load->view("layout/footer");
?> 

<script type='text/javascript'>
          $('#table_regional').dataTable();
        $(document).ready(function(){
            // $( "#nama_region" ).autocomplete({
            //   source: "<?php //echo site_url('regional/get_autocomplete');?>"
            // });
          $('.auto_search_region').autocomplete({
               serviceUrl: '<?php echo site_url();?>regional/get_autocomplete',
               // onSelect: function (suggestion) {
               //      $('#cari_no_cm').val(''+suggestion.no_cm);
               //      $('#no_medrec_baru').val(''+suggestion.no_medrec);
               //      // alert(suggestion.no_cm);

               // }
          });
        });

        
        function insert_regional() {
          document.getElementById("btn-submit").innerHTML = '<i class="fa fa-spinner fa-spin"></i> Processing...';
          $.ajax({
               type: "POST",
               url: "<?php echo site_url('regional/insert_regional'); ?>",
               dataType: "JSON",
               data: $('#cek_form').serialize(),
               success: function(data, result) {
                    if (data == true) {
                         document.getElementById("btn-submit").innerHTML = 'Simpan';
                         swal.fire({
                              title: 'Data berhasil disimpan',
                              type: 'success',
                              showCancelButton: false,
                              confirmButtonText: 'Ok',
                              confirmButtonClass: 'btn btn-primary',
                              buttonsStyling: false,
                         }).then(function(result) {
                              window.location.reload();
                         });
                    } else {
                              document.getElementById("btn-submit").innerHTML = 'Simpan';
                              Swal.fire({
                                   type: 'warning',
                                   title: 'Gagal!',
                                   text: data.msg
                              });
                         }
                    },
               error: function(event, textStatus, errorThrown) {
                    document.getElementById("btn-submit").innerHTML = 'Simpan';
                    swal.fire("Error", "Gagal menyimpan data.", "error");
                    console.log('Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown);
               }
          });
     }
</script>