<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once("Secure_area.php");
class Home extends Secure_area {
	public function __construct() {
		parent::__construct();

		$this->load->model('admin/M_user','',TRUE);
		$this->load->model('admin/M_sekolah','',TRUE);
		// $this->load->helper('pdf_helper');
	}
	
	public function index(){
		$data['title'] = 'Home';
        $this->load->view('home/index',$data);
    }
}