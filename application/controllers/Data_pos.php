<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once("Secure_area.php");
class Data_pos extends Secure_area
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin/M_user', '', TRUE);
        $this->load->model('admin/Mdata_pos', '', TRUE);
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['data_pos'] = $this->Mdata_pos->get_alldata()->result();
        $this->load->view('datapos/index', $data);
    }

    public function tambah()
    {
        $data['title'] = 'Tambah Data';
        $data['station'] = $this->Mdata_pos->get_station_type()->result();
        $data['province'] = $this->Mdata_pos->get_province()->result();
        $this->load->view('datapos/tambah_data', $data);
    }

    public function data_city($id = '')
	{
		$data = $this->Mdata_pos->get_city($id)->result();
		echo "<option value=''>Pilih Kota</option>";
		foreach ($data as $row) {
			echo "<option value='" . $row->id . "'>$row->name</option>";
		}
    }
    
    public function data_sub()
	{
        $id_prov = $this->input->post('id_prov');
        $id_kota = $this->input->post('id_kota');
        $name = '';
		$data = $this->Mdata_pos->get_sub($id_kota, $id_prov)->result();
        foreach ($data as $row) {
			if ($row->name != '') {
				$name .= "<option value='" . $row->id . "'> $row->name </option>";
			} else {
				$name .= "<option value='" . $row->id . "'> $row->name </option>";
			}
		}
		if ($name != "") {
			echo json_encode(array('success' => true, 'data' => $name));
		} else {
			echo json_encode(array('success' => false));
		}
    }

    public function data_vill()
	{
        $id_prov = $this->input->post('id_prov');
        $id_kota = $this->input->post('id_kota');
        $id_sub = $this->input->post('id_sub');
        $name = '';
		$data = $this->Mdata_pos->get_vill($id_sub, $id_kota, $id_prov)->result();
        foreach ($data as $row) {
			if ($row->name != '') {
				$name .= "<option value='" . $row->id . "'> $row->name </option>";
			} else {
				$name .= "<option value='" . $row->id . "'> $row->name </option>";
			}
		}
		if ($name != "") {
			echo json_encode(array('success' => true, 'data' => $name));
		} else {
			echo json_encode(array('success' => false));
		}
    }
    

    public function tambah_datapos()
    {
        $config['upload_path'] = './assets/uploads/'; //path folder
        $config['allowed_types'] = '*'; //Allowing types
        $config['encrypt_name'] = false; //encrypt file name 

        $this->upload->initialize($config);
        if (!empty($_FILES['filefoto']['name'])) {

            if ($this->upload->do_upload('filefoto')) {

                $data   = $this->upload->data();
                $name  = $this->input->post('nama');
                $das  = $this->input->post('das');
                $station_type  = $this->input->post('tipe_stat');
                $latitude  = $this->input->post('latitude');
                $longitude  = $this->input->post('longitude');
                $province_id  = $this->input->post('id_prov');
                $city_id  = $this->input->post('id_kota');
                $subdistrict_id  = $this->input->post('id_sub');
                $village_id  = $this->input->post('id_vill');
                $gambar  = $data['file_name']; //get file name
                $created_at  = date("Y-m-d H:i:s");
                $created_by  = $this->load->get_var("user_info")->userid;
                $this->Mdata_pos->insert_pos($name, $das, $station_type, $latitude, $longitude, $province_id, $city_id, $subdistrict_id, $village_id, $gambar, $created_at, $created_by);
                $success =  '<div class="alert alert-success mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <h4 class="text-success"><i class="fa fa-check"></i> Pos berhasil dibuat.</h4>
                        </div>';
                $this->session->set_flashdata('success_msg', $success);
                redirect('Data_pos/tambah');
            } else {
                $success =  '<div class="alert alert-danger mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <h4 class="text-danger"><i class="fa fa-ban"></i> Upload gagal, file tidak mendukung.</h4>
                        </div>';
                $this->session->set_flashdata('success_msg', $success);
                redirect('Data_pos/tambah');
            }
        } else {
            $success =  '<div class="alert alert-danger mt-1">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <h4 class="text-danger"><i class="fa fa-ban"></i> Gagal, file kosong.</h4>
                        </div>';
            $this->session->set_flashdata('success_msg', $success);
            redirect('Data_pos/tambah');
        }
    }

    function get_data_pos()
    {
        $id = $this->input->post('id');
        $datajson = $this->Mdata_pos->get_data_pos($id)->result();
        echo json_encode($datajson);
    }

    function edit_datapos()
    {
        $config['upload_path'] = './assets/upload/'; //path folder
        $config['allowed_types'] = '*'; //Allowing types
        $config['encrypt_name'] = false; //encrypt file name 

        $this->upload->initialize($config);

        $where = array('id' => $this->input->post('edit_id'));
        $data   = $this->upload->data();
        $judul  = $this->input->post('edit_judul');
        $deskripsi  = $this->input->post('edit_deskripsi');
        $informasi  = $this->input->post('edit_informasi');
        $gambar  = $data['file_name']; //get file name
        $latitude  = $this->input->post('edit_latitude');
        $longitude  = $this->input->post('edit_longitude');
        $this->Mdata_pos->update_pos($judul, $deskripsi, $informasi, $gambar, $latitude, $longitude, $where);
        $success =  '<div class="alert alert-success mt-1">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                <h4 class="text-success"><i class="fa fa-check"></i> Pos berhasil diubah.</h4>
                            </div>';
        $this->session->set_flashdata('success_msg', $success);
        redirect('Data_pos');
    }

    function delete_pos()
    {
        $id = $this->input->post('id');
        $result = $this->Mdata_pos->delete_pos($id);

        echo json_decode($result);
    }
}
