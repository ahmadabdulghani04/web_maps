<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_sekolah extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function insert_dyn_user($data)
	{
		$this->db->insert('dyn_user', $data);
		return $this->db->insert_id();
	}

	function update_dyn_user($data, $userid)
	{
		$this->db->where('userid', $userid);
		$this->db->update('dyn_user', $data);
		return true;
	}

	function exist($username)
	{
		$this->db->from('dyn_user');
		$this->db->where('username', $username);
		return $this->db->count_all_results();
	}

	function check_pass_match($data)
	{
		$this->db->from('dyn_user');
		$this->db->where('userid', $this->session->userdata('userid'));
		$this->db->where('password', $data['currpass']);
		return $this->db->count_all_results();
	}

	function get_all()
	{
		return $this->db->query("select *
			from dyn_user
			where deleted = 0
			order by username asc");
	}

	function get_all_roleid($roleid)
	{
		return $this->db->query("select *
			from dyn_user
			where roleid = '$roleid'
			order by username asc");
	}

	function get_all_superadmin()
	{
		return $this->db->query("select *
			from dyn_user
			where roleid = 1
			order by username asc");
	}

	function getall_role()
	{
		return $this->db->query("SELECT*FROM dyn_role WHERE id!=1 ORDER BY id ASC");
	}

	function get_role_all($id)
	{
		return $this->db->query("SELECT b.id, IFNULL(a.roleid,0) as sts, b.role
			from dyn_user a
			RIGHT JOIN 
			dyn_role b
			on a.roleid = b.id
			and a.userid = $id");
	}
	function get_role_id()
	{
		$userid = $this->session->userdata('userid');
		return $this->db->query("Select roleid from dyn_user where userid = '" . $userid . "'");
	}

	function get_role_gudang($id)
	{
		return $this->db->query("select b.id_gudang, IFNULL(a.id_gudang,0) as sts, b.nama_gudang
			from dyn_gudang_user a
			RIGHT JOIN 
			master_gudang b
			on a.id_gudang = b.id_gudang
			and a.userid = $id");
	}

	function get_role_poli($id)
	{
		return $this->db->query("select b.id_poli, IFNULL(a.id_poli,0) as sts, b.nm_poli
			from dyn_poli_user a
			RIGHT JOIN 
			poliklinik b
			on a.id_poli = b.id_poli
			and a.userid = $id");
	}

	function get_role_ruang($id)
	{
		return $this->db->query("select b.idrg as id_ruang, IFNULL(a.id_ruang,0) as sts, b.nmruang as nm_ruang
			from dyn_ruang_user a
			RIGHT JOIN 
			ruang b
			on a.id_ruang = b.idrg
			and a.userid = $id");
	}

	function get_role_akses($id)
	{
		return $this->db->query("select b.id, b.deskripsi, b.id as idkasir, IFNULL(a.idkasir,0) as sts, b.kasir
			from dyn_kasir_user a
			RIGHT JOIN 
			dyn_kasir b
			on a.idkasir = b.id
			and a.userid = $id");
	}

	function get_role_aksesOne($id)
	{
		return $this->db->query("select b.id, b.deskripsi, b.id as idkasir, IFNULL(a.idkasir,0) as sts, b.kasir
			from dyn_kasir_user a
			JOIN 
			dyn_kasir b
			on a.idkasir = b.id
			and a.userid = $id");
	}
	/*
	Attempts to login employee and set session. Returns boolean based on outcome.
	*/
	function login($username, $password)
	{
		//$query = $this->db->get_where('dyn_user', array('username' => $username,'password'=>md5($password), 'deleted'=>0), 1);
		$query = $this->db->get_where('dyn_user', array('username' => $username, 'password' => $password, 'deleted' => 0), 1);
		if ($query->num_rows() == 1) {
			$row = $query->row();
			$this->session->set_userdata('userid', $row->userid);
			return true;
		}
		return false;
	}

	/*
	Logs out a user by destorying all session data and redirect to login
	*/
	function logout()
	{
		$this->session->sess_destroy();
		redirect('loginn');
	}

	/*
	Determins if a user is logged in
	*/
	function is_logged_in()
	{
		return $this->session->userdata('userid') != false;
	}
	/*
	Gets information about a user loged in
	*/
	function get_logged_in_user_info()
	{
		$userid = $this->session->userdata('userid');
		if (($userid)) {
			return $this->get_info($userid);
		}
	}
	/*
	Gets information about a particular user
	*/
	function get_info($userid)
	{
		$this->db->select('userid, name, username, roleid, id_klinik, nama as nm_klinik, namasingkat as namasingkat_klinik, provinsi as provinsi_klinik, kotakabupaten as kotakabupaten_klinik, kecamatan as kecamatan_klinik, kelurahandesa as kelurahandesa_klinik, alamat as alamat_klinik, telp as telp_klinik, email as email_klinik, logo as logo_klinik');
		$this->db->from('dyn_user');
		$this->db->where('userid', $userid);
		$this->db->join('data_klinik', 'data_klinik.id = dyn_user.id_klinik', 'left');
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			//Get empty base parent object, as $item_id is NOT an item
			$data_obj = new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('dyn_user');

			foreach ($fields as $field) {
				$data_obj->$field = '';
			}

			return $data_obj;
		}
	}
	/*
	Determins whether the employee specified employee has access the specific module.
	*/
	function has_permission($url, $userid)
	{
		//if no module_id is null, allow access
		if ($url == null or $url == 'beranda' or $url == 'logout') {
			return true;
		} else {
			if ($this->is_menu($url)) {
				$query = $this->db->query("select count(*) as jml
					from dyn_user ru, dyn_role_menu rm, dyn_menu m
					where ru.userid = $userid
						and ru.roleid = rm.role_id
					  and rm.menu_id = m.page_id
					  and m.url = '$url'");
				return ($query->row()->jml > 0);
			} else {
				return true;
			}
		}
		return false;
	}

	function is_menu($url)
	{
		$query = $this->db->query("select count(show_menu) as jml
					from dyn_menu
					where url = '$url' and show_menu = 1");
		return ($query->row()->jml == 1);
	}

	function has_gudang_access($userid, $id_gudang)
	{
		$query = $this->db->query("select count(*) as jml
					from dyn_gudang_user
					where userid = $userid and id_gudang = $id_gudang");
		return $query->row()->jml;
	}

	function has_poli_access($userid, $id_poli)
	{
		$query = $this->db->query("select count(*) as jml
					from dyn_poli_user
					where userid = $userid and id_poli = $id_poli");
		return $query->row()->jml;
	}

	function has_ruang_access($userid, $id_ruang)
	{
		$query = $this->db->query("select count(*) as jml
					from dyn_ruang_user
					where userid = $userid and id_ruang = $id_ruang");
		return $query->row()->jml;
	}

	function save(&$data, $foto)
	{
		$query = $this->db->query("INSERT INTO dyn_user (username, password, name, deleted, foto) VALUES ('" . $data["username"] . "','" . $data["password"] . "','" . $data["name"] . "','0','$foto')");
		if ($query) {
			return true;
		}
		return false;
	}
	function update($data)
	{
		$this->db->set('password', $this->phpass->hash($data["vpassword"]));
		$this->db->where('userid', $data["vuserid"]);
		return $this->db->update('dyn_user');
	}
	// function delete($id)
	// {
	// 	$this->db->where('userid',$id);	
	// 	if ($this->db->delete('dyn_user')){
	// 		return true;
	// 	}else{			
	// 		return false;
	// 	}
	// }
	function delete($id)
	{
		$this->db->set('deleted', '1');
		$this->db->where('userid', $id);
		if ($this->db->update('dyn_user')) {
			return true;
		} else {
			return false;
		}
	}
	function active($id)
	{
		$this->db->set('deleted', '0');
		$this->db->where('userid', $id);
		if ($this->db->update('dyn_user')) {
			return true;
		} else {
			return false;
		}
	}
	function userRoleSave($id, &$data)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_role_user');
		$temp = count($data);
		for ($i = 0; $i < $temp; $i++) {
			$this->db->insert('dyn_role_user', $data[$i]);
		}
		return true;
	}
	function userAksesSave($id, &$data)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_kasir_user');
		$temp = count($data);
		for ($i = 0; $i < $temp; $i++) {
			$this->db->insert('dyn_kasir_user', $data[$i]);
		}
		return true;
	}

	function userAksesSaveOne($id, $data)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_kasir_user');

		$this->db->insert('dyn_kasir_user', $data);

		return true;
	}

	function userGdgSave($id, &$data)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_gudang_user');
		$temp = count($data);
		for ($i = 0; $i < $temp; $i++) {
			$this->db->insert('dyn_gudang_user', $data[$i]);
		}
		return true;
	}
	function userGdgDelete($id)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_gudang_user');
		return true;
	}

	function getKasirAkses($id)
	{
		$this->db->where('userid', $id);
		$this->db->from('dyn_kasir_user');
		return $this->db->get()->row();
	}

	function getBukaAkses($id, $id_klinik)
	{
		$this->db->where('userid', $id);
		$this->db->where('id_klinik', $id_klinik);
		$this->db->where('tgl_tutup', null);
		$this->db->select('count(id_transaksi) as status');
		$this->db->from('buka_transaksi');
		return $this->db->get()->row();
	}

	function userPoliSave($id, &$data)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_poli_user');
		$temp = count($data);
		for ($i = 0; $i < $temp; $i++) {
			$this->db->insert('dyn_poli_user', $data[$i]);
		}
		return true;
	}
	function userPoliDelete($id)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_poli_user');
		return true;
	}
	function userRuangSave($id, &$data)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_ruang_user');
		$temp = count($data);
		for ($i = 0; $i < $temp; $i++) {
			$this->db->insert('dyn_ruang_user', $data[$i]);
		}
		return true;
	}
	function userRuangDelete($id)
	{
		$this->db->where('userid', $id);
		$this->db->delete('dyn_ruang_user');
		return true;
	}


	function update_photo($uid, $foto)
	{
		return $this->db->query("update dyn_user set foto = '" . $foto . "' where username = '" . $uid . "'");
	}
	function update_name($data)
	{
		return $this->db->query("update dyn_user set name = '" . $data["uname"] . "' where username = '" . $data["uid"] . "'");
	}
	// function change_pass($data){
	// 	return $this->db->query("update dyn_user set password = '".$data["newpass"]."' where userid = '".$this->session->userdata('userid')."'");
	// }
	function change_pass($data)
	{
		$hash_pwd = $this->phpass->hash($data["newpass"]);
		return $this->db->query("update dyn_user set password = '" . $hash_pwd . "' where userid = '" . $this->session->userdata('userid') . "'");
	}
	function get_user($username)
	{
		return $this->db->query("SELECT * from dyn_user where deleted = 0 and username COLLATE latin1_general_cs LIKE '$username'");
	}
	function get_nisn($nisn)
	{
		return $this->db->query("SELECT * from siswa where nisn='$nisn'");
	}
	function get_siswa()
	{
		return $this->db->query("SELECT * from siswa");
	}
	function insert_sekolah($data)
	{
		$this->db->insert('user_sekolah', $data);

		return true;
	}

	function insert_region($data)
	{
		$this->db->insert('dyn_user', $data);

		return true;
	}

	function insert_siswa($data)
	{
		$this->db->insert('siswa', $data);

		return true;
	}

	function get_sekolah()
	{
		return $this->db->query("SELECT * FROM master_sekolah");
	}

	function get_sekolah_by_id_sekolah($id_sekolah)
	{
		return $this->db->query("SELECT * FROM master_sekolah WHERE id_sekolah='$id_sekolah'");
	}

	function get_nuptk($nuptk)
	{
		return $this->db->query("SELECT * FROM user_sekolah WHERE nuptk = '" . $nuptk . "' ");
	}

	function cek_sekolah($sekolah)
	{
		return $this->db->query("SELECT * FROM user_sekolah WHERE sekolah = '" . $sekolah . "' AND statuss = '1' ");
	}

	function cek_sekolah2($sekolah)
	{
		return $this->db->query("SELECT * FROM master_sekolah WHERE nm_sekolah = '" . $sekolah . "' ");
	}

	function cek_siswa($nisn)
	{
		return $this->db->query("SELECT * FROM master_siswa WHERE nisn = '" . $nisn . "' ");
	}

	function cek_nik($nisn, $nik)
	{
		return $this->db->query("SELECT * FROM master_siswa WHERE nisn = '" . $nisn . "' AND nik = '" . $nik . "' ");
	}

	function cek_npsn($nisn, $npsn)
	{
		return $this->db->query("SELECT * FROM master_siswa WHERE nisn = '" . $nisn . "' AND npsn = '" . $npsn . "' ");
	}

	function cek_email($email)
	{
		return $this->db->query("SELECT * FROM siswa WHERE email = '" . $email . "' ");
	}
	function get_region()
	{
		return $this->db->query("SELECT * FROM master_region");
	}
	function get_region_by_id($id_region)
	{
		return $this->db->query("SELECT * FROM master_region WHERE id_region='$id_region'");
	}
	function get_sekolah_by_region($id_region)
	{
		return $this->db->query("SELECT * FROM master_sekolah WHERE id_region='$id_region'");
	}
	function get_filtered_siswa($id_region, $id_sekolah)
	{
		$query = '';
		if ($id_sekolah != 'null') {
			$query = "AND id_sekolah='$id_sekolah'";
		}
		return $this->db->query("SELECT * FROM siswa WHERE id_region='$id_region' $query ");
	}
	function get_data_pendaftar_by_region($id_region)
	{
		return $this->db->query("SELECT siswa.nm_sekolah, count(id) as siswa from siswa join master_sekolah on siswa.id_sekolah=master_sekolah.id_sekolah where master_sekolah.id_region='$id_region' group by siswa.id_sekolah");
	}

	function get_all_pendaftar_by_region($id_region)
	{
		return $this->db->query("SELECT master_sekolah.nm_sekolah, count(id) as siswa from master_sekolah left join siswa on siswa.id_sekolah=master_sekolah.id_sekolah where master_sekolah.id_region='$id_region' GROUP BY master_sekolah.id_sekolah");
	}
	function get_all_pendaftar_by_jenjang($id_region)
	{
		return $this->db->query("SELECT master_sekolah.jenjang, count(id) as siswa from master_sekolah left join siswa on siswa.id_sekolah=master_sekolah.id_sekolah where master_sekolah.id_region='$id_region' GROUP BY master_sekolah.jenjang");
	}

	function get_perbandingan_daftar_sekolah($id_sekolah)
	{
		return $this->db->query("SELECT count(master_siswa.nisn) as seluruh, count(siswa.nisn) as daftar  from master_siswa left join siswa on master_siswa.nisn=siswa.nisn where master_siswa.id_sekolah='$id_sekolah'");
	}

	function get_user_sekolah()
	{
		return $this->db->query("SELECT * FROM user_sekolah");
	}

	function get_asal_sekolah($userid, $role_id)
	{
		if ($role_id == 10) {
			return $this->db->query("SELECT '' as id_sekolah,id_region FROM master_region join dyn_user on master_region.id_region = dyn_user.id_regional where userid ='$userid'");
		} else if ($role_id == 9) {
			return $this->db->query("SELECT master_sekolah.id_sekolah,id_region FROM master_sekolah join dyn_user on master_sekolah.id_sekolah = dyn_user.id_sekolah where userid ='$userid'");
		} else {
			return $this->db->query("SELECT id_sekolah,id_regional as id_region FROM  dyn_user where userid ='$userid'");
		}
	}

	function update_to_active($id)
	{
		$data = array(
			"statuss" => '1'
		);

		$this->db->where('id', $id);
		$this->db->update('user_sekolah', $data);
	}

	function update_to_nonactive($id)
	{
		$data = array(
			"statuss" => '0'
		);

		$this->db->where('id', $id);
		$this->db->update('user_sekolah', $data);
	}

	function search_blog($title)
	{
		$this->db->like('nm_sekolah', $title, 'both');
		$this->db->order_by('nm_sekolah', 'ASC');
		$this->db->limit(10);
		return $this->db->get('master_sekolah')->result();
	}

	function search_regional($title)
	{
		$this->db->like('nama_region', $title, 'both');
		$this->db->order_by('nama_region', 'ASC');
		$this->db->limit(10);
		return $this->db->get('master_region')->result();
	}

	function get_sekolah_by_id($id)
	{
		return $this->db->query("SELECT * FROM user_sekolah WHERE id = '$id'");
	}

	function get_siswa2($id_sekolah)
	{
		return $this->db->query("SELECT * from siswa WHERE id_sekolah = '$id_sekolah'");
	}

	function get_roleid($roleid)
	{
		return $this->db->query("SELECT roleid FROM dyn_user where roleid = '$roleid'");
	}

	function cek_npsn_by_id($nisn, $npsn, $id_sekolah)
	{
		return $this->db->query("SELECT * FROM master_siswa WHERE nisn = '" . $nisn . "' AND npsn = '" . $npsn . "' AND id_sekolah = '$id_sekolah'");
	}

	function cek_siswa_by_id($nisn, $id_sekolah)
	{
		return $this->db->query("SELECT * FROM master_siswa WHERE nisn = '" . $nisn . "' AND id_sekolah = '$id_sekolah'");
	}

	function cek_region($nama_region)
	{
		return $this->db->query("SELECT * FROM master_region WHERE nama_region = '$nama_region'");
	}

	function get_regions()
	{
		return $this->db->query("SELECT * FROM master_region ");
	}

	function get_user_region()
	{
		return $this->db->query("SELECT * FROM dyn_user a 
								LEFT JOIN master_region b 
									on b.id_region=a.id_regional
									WHERE roleid = 10");
	}

	function log_activity($data)
	{
		return $this->db->insert('log_activity', $data);
	}

	function get_userid($username)
	{
		// return $this->db->query("SELECT userid FROM dyn_user WHERE username='$username'");
		$this->db->select('userid')->from('dyn_user')->where('username', $username);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->row()->userid;
		}
		return false;
	}

	function last_login($id)
	{
		return $this->db->query("SELECT *
		FROM log_activity a LEFT JOIN dyn_user b ON b.userid=a.userid
		WHERE a.userid = '$id' ");
	}

	function get_iduser()
	{
		return $this->db->query("SELECT userid FROM dyn_user");
	}

	function dyn_user($username)
	{
		return $this->db->query("SELECT * FROM dyn_user WHERE username = '" . $username . "'");
	}

	function jns_kel($id_sekolah) {
		return $this->db->query("SELECT sum(if((sex = 'L'),1,0)) as laki, sum(if((sex = 'P'),1,0)) as perem FROM siswa WHERE id_sekolah='$id_sekolah'");
	}

	function jns_kel2() {
		return $this->db->query("SELECT sum(if((sex = 'L'),1,0)) as laki, sum(if((sex = 'P'),1,0)) as perem FROM siswa");
	}
}
