<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once("Secure_area.php");
class Regional extends Secure_area
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/M_sekolah', '', TRUE);
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Regional'
        );
        $data['data_region'] = $this->M_sekolah->get_user_region()->result();
        $this->load->view('regional/formregional', $data);
    }

    function get_autocomplete()
    {
        // if (isset($_GET['term'])) {
        //     $result = $this->M_sekolah->search_regional($_GET['term']);
        //     if (count($result) > 0) {
        //         foreach ($result as $row)
        //             $arr_result[] = $row->id_region . ' - ' .$row->nama_region;
        //         echo json_encode($arr_result);
        //     }
        // }
        $keyword = $this->input->get("query");
        $data = $this->M_sekolah->search_regional($keyword);

        $arr = [];
        $arr['suggestions'] = [];
        foreach ($data as $row) {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' => $row->id_region . ' - ' . $row->nama_region
            );
        }
        // minimal PHP 5.2
        echo json_encode($arr);
    }

    function insert_regional()
    {
        $cek = explode('-', $this->input->post('nama_region'));
        $data = array(
            'name' => $this->input->post('name'),
            'username' => $this->input->post('username'),
            'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
            'id_regional' => $cek[0],
            'roleid' => '10',
            'xcreate_date' => date('Y-m-d h:i:s'),
            'xcreate_user' => $this->load->get_var("user_info")->username
        );

        $data1 = array(
            'keterangan' => 'create regional',
            'ket_waktu' => date('Y-m-d h:i:s'),
            'userid' => $this->load->get_var('user_info')->userid
        );
        $cekk = $this->M_sekolah->dyn_user($data['username'])->num_rows();
        if ($cekk > 0) {
            $result = array('success' => false, 'msg' => 'Username Sudah Ada');
        } else {
            $result = array('success' => true);
            $result = $this->M_sekolah->insert_region($data);
            $result = $this->M_sekolah->log_activity($data1);
        }
        echo json_encode($result);
    }
}
