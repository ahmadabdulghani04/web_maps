<?php

class Mdata_pos extends CI_Model
{

    public function get_alldata()
    {
        return $this->db->query("SELECT a.*, b.name AS prov, c.name as sub, d.name as villa, e.name as city FROM stations a LEFT JOIN provinces b ON a.province_id=b.id LEFT JOIN subdistricts c ON a.subdistrict_id=c.id LEFT JOIN villages d ON a.village_id=d.id LEFT JOIN cities e ON a.city_id=e.id");
    }
    public function get_image()
    {
        return $this->db->get("stations");
    }

    public function insert_pos($name, $das, $station_type, $latitude, $longitude, $province_id, $city_id, $subdistrict_id, $village_id, $gambar, $created_at, $created_by)
    {
        $data = array(
            'name' => $name,
            'das' => $das,
            'station_type' => $station_type,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'province_id' => $province_id,
            'city_id' => $city_id,
            'subdistrict_id' => $subdistrict_id,
            'village_id' => $village_id,
            'featured_image' => $gambar,
            'created_at' => $created_at,
            'created_by' => $created_by,
        );
        $result = $this->db->insert('stations', $data);
        return $result;
    }

    function get_data_pos($id)
    {
        $this->db->select('*');
        $this->db->from('data_pos');
        $this->db->where('id', $id);
        return $this->db->get();
    }

    function update_pos($judul, $deskripsi, $informasi, $gambar, $latitude, $longitude, $where)
    {
        $data = array(
            'judul' => $judul,
            'deskripsi' => $deskripsi,
            'informasi' => $informasi,
            'gambar' => $gambar,
            'latitude' => $latitude,
            'longitude' => $longitude,
        );
        return $this->db->update('data_pos', $data, $where);
    }

    function delete_pos($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('data_pos');

        return true;
    }

    function get_station_type(){
        $this->db->select('*');
        $this->db->from('stationtypes');
        return $this->db->get();
    }

    function get_province(){
        $this->db->select('*');
        $this->db->from('provinces');
        return $this->db->get();
    }

    function get_city($id){
        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('province_id', $id);
        return $this->db->get();
    }

    function get_sub($id, $id_prov){
        $this->db->select('a.id, a.name as name');
        $this->db->from('subdistricts a');
        $this->db->join('cities b', 'a.city_id=b.id');
        $this->db->join('provinces c', 'b.province_id=c.id');
        $this->db->where('a.city_id', $id);
        $this->db->where('b.province_id', $id_prov);
        return $this->db->get();
    }

    function get_vill($id, $id_city, $id_prov){
        $this->db->select('a.id, a.name as name');
        $this->db->from('villages a');
        $this->db->join('subdistricts b', 'a.subdistrict_id=b.id');
        $this->db->join('cities c', 'b.city_id=c.id');
        $this->db->join('provinces d', 'c.province_id=d.id');
        $this->db->where('a.subdistrict_id', $id);
        $this->db->where('b.city_id', $id_city);
        $this->db->where('c.province_id', $id_prov);
        return $this->db->get();
    }

    function get_detail($id){
        return $this->db->query("SELECT a.*, b.name AS prov, c.name as sub, d.name as villa, e.name as city, f.name as station FROM stations a LEFT JOIN provinces b ON a.province_id=b.id LEFT JOIN subdistricts c ON a.subdistrict_id=c.id LEFT JOIN villages d ON a.village_id=d.id LEFT JOIN cities e ON a.city_id=e.id LEFT JOIN stationtypes f ON a.station_type=f.id WHERE a.id='$id'");
    }

    function get_hidrologi(){
        return $this->db->query("SELECT a.id, a.name as nama_pos, b.name as kategori FROM stations a LEFT JOIN stationtypes b ON a.station_type=b.id");
    }

    function get_hidrologi_2020($id){
        return $this->db->query("SELECT a.*, b.name AS prov, c.name as sub, d.name as villa, e.name as city,f.name as station FROM stations a LEFT JOIN provinces b ON a.province_id=b.id LEFT JOIN subdistricts c ON a.subdistrict_id=c.id LEFT JOIN villages d ON a.village_id=d.id LEFT JOIN cities e ON a.city_id=e.id LEFT JOIN stationtypes f ON a.station_type=f.id WHERE a.id='$id' AND SUBSTR(created_at, 1,4) = 2020");
    }
}
